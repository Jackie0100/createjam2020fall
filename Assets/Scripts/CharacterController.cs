﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterController : MonoBehaviour
{
    [SerializeField]
    private LayerMask PlatformsLayerMask;
    [SerializeField]
    private LayerMask PickupsLayerMask;
    [SerializeField]
    private LayerMask EnemyLayerMask;

    [SerializeField]
    private Animator animator;

    [SerializeField]
    private float MoveSpeed;
    [SerializeField]
    private float DashDistance;
    [SerializeField]
    private float JumpVelocity;
    [SerializeField]
    private float ThrowStrength;

    [SerializeField]
    private int MaxHealth;
    [SerializeField]
    private int HealthRegen;
    [SerializeField]
    private float TimeBeforeRest;

    [SerializeField]
    private float TimeBetweenAttacks;
    [SerializeField]
    private float AttackRange;
    [SerializeField]
    public int AttackDamage;
    [SerializeField]
    private GameObject image;

    // Movement
    private Rigidbody2D rigidbody2D;
    private CapsuleCollider2D capsuleCollider2D;
    private Vector3 moveDir;
    private bool isJumpButtonDown;
    private bool isDashButtonDown;

    // Combat
    private bool alive;
    private bool resting;
    private bool attacking;
    private int currentHealth;
    // Economy
    private int currentGold;

    public int CurrentHealth
    {
        get { return currentHealth; }
        set
        {
            currentHealth = Mathf.Min(value, MaxHealth);
            image.GetComponent<HPBarManager>().ChangeBar(currentHealth);
            if (currentHealth <= 0)
            {
                alive = false;
                animator.SetTrigger("Death");
                StartCoroutine("OnDeath");
            }
            if(!alive && currentHealth > 0)
            {
                alive = true;
                animator.SetTrigger("Revive");
            }
        }
    }
    public int CurrentGold
    {
        get { return currentGold; }
        set
        {
            currentGold = value;
            Debug.Log(currentGold);
        }
    }

    private void Awake()
    {
        alive = true;
        rigidbody2D = GetComponent<Rigidbody2D>();
        capsuleCollider2D = GetComponent<CapsuleCollider2D>();       
    }

    private void Update()
    {
        if (alive)
        {
            // Walk
            moveDir.x = Input.GetAxis("Horizontal");
            if (moveDir != Vector3.zero)
            {
                float angle = Mathf.Atan2(moveDir.y, moveDir.x) * Mathf.Rad2Deg;
                transform.localRotation = Quaternion.AngleAxis(angle, Vector3.up);
            }

            // Jump
            if (Input.GetButtonDown("Jump") && IsOnGround())
            {
                isJumpButtonDown = true;

                animator.SetTrigger("Jump");
                StopCoroutine("InAction");
                StartCoroutine("InAction");
            }

            // Dash
            if (Input.GetButtonDown("Dash") && IsOnGround())
            {
                isDashButtonDown = true;

                StopCoroutine("InAction");
                StartCoroutine("InAction");
            }

            // Attack
            if (Input.GetButtonDown("Fire1"))
            {
                if (!attacking)
                {
                    animator.SetTrigger("Attack");
                    StartCoroutine("Attack");
                    StopCoroutine("InAction");
                    StartCoroutine("InAction");
                }
            }
        }
        else
        {
            // show start menu
        }
    }

    
    private void FixedUpdate()
    {
        float fallPenalty = 1;
        // Walk
        if (!IsOnGround())
            fallPenalty = .3f;
        rigidbody2D.velocity = moveDir * MoveSpeed *fallPenalty;
        animator.SetFloat("Speed", Mathf.Abs(rigidbody2D.velocity.x));

        // Jump
        if (isJumpButtonDown)
        {
            rigidbody2D.AddForce((moveDir + Vector3.up) * JumpVelocity, ForceMode2D.Impulse);
            isJumpButtonDown = false;
        }

        // Dash
        if (isDashButtonDown)
        {
            rigidbody2D.MovePosition(transform.position + moveDir * DashDistance);
            isDashButtonDown = false;
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.gameObject.CompareTag("Pickup"))
            return;
        CurrentGold += 1;
        Destroy(other.gameObject);
    }

    // Attacking functions
    #region
    private IEnumerator Attack()
    {
        attacking = true;       

        GameObject target = EnemiesInRange();
        if (target != null)
        {
            Debug.Log(string.Format("Attacking {0}", target.name));
            target.GetComponent<Enemy>().TakeDamage(AttackDamage);
        }

        yield return new WaitForSeconds(TimeBetweenAttacks);

        attacking = false;
    }

    public void TakeDamage(int damage)
    {
        CurrentHealth -= damage;
    }

    public void FullHealth()
    {
        CurrentHealth = MaxHealth;
    }

    private GameObject EnemiesInRange()
    {
        RaycastHit2D raycastHit2D = Physics2D.BoxCast(capsuleCollider2D.bounds.center, capsuleCollider2D.bounds.size, 0f, transform.right, AttackRange, EnemyLayerMask);        
        if(raycastHit2D.collider != null)
            return raycastHit2D.collider.gameObject;
        return null;
    }
    #endregion

    private bool IsOnGround()
    {
        RaycastHit2D raycastHit2D =  Physics2D.BoxCast(capsuleCollider2D.bounds.center, capsuleCollider2D.bounds.size, 0f, Vector3.down, .1f, PlatformsLayerMask);       
        return raycastHit2D.collider != null;
    }
    // Is in Action/combat, resting when out of action
    #region
    private IEnumerator InAction()
    {
        resting = false;
        StopCoroutine("Resting");
        yield return new WaitForSeconds(TimeBeforeRest);

        resting = true;
        StartCoroutine("Resting");
    }

    private IEnumerator Resting()
    {
        while (resting)
        {
            CurrentHealth += HealthRegen;

            yield return new WaitForSeconds(1);
        }       
    }

    private IEnumerator OnDeath()
    {
        // Play Death Animation

        // Show End Screen

        yield return new WaitForSeconds(1);
    }
    #endregion

    void OnDrawGizmosSelected()
    {
        capsuleCollider2D = GetComponent<CapsuleCollider2D>();
        // Draw a semitransparent blue cube at the transforms position
        Gizmos.color = new Color(1, 0, 0, 0.5f);        
        Vector3 center = capsuleCollider2D.bounds.center + transform.right*AttackRange;
        Gizmos.DrawCube(center, capsuleCollider2D.bounds.size);
    }
}
