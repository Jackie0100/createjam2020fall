﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace createjam
{
    public class TimeOfDaySystem : StateMachine
    {
        [SerializeField]
        private GameObject player;
        [SerializeField]
        private GameObject troll;
        [SerializeField]
        private GameObject shop;
        [SerializeField]
        private GameObject startScreen;
        [SerializeField]
        private GameObject startScreenUI;
        [SerializeField]
        private GameObject endScreen;
        [SerializeField]
        private GameObject endScreenUI;
        [SerializeField]
        private GameObject lightTheme;
        [SerializeField]
        private GameObject darkTheme;
        [SerializeField]
        public GameObject DayMusic;
        [SerializeField]
        public GameObject NightMusic;
        [SerializeField]
        public GameObject events;
        [SerializeField]
        public Text textUI;

        public int TrollsNextTurn = 6;
        public long score;

        public GameObject LightTheme
        {
            get { return lightTheme; }
        }
        public GameObject DarkTheme
        {
            get { return darkTheme; }
        }
        public GameObject StartScreen
        {
            get { return startScreen; }
        }        
        public GameObject StartScreenUI
        {
            get { return startScreenUI; }
        }
        public GameObject EndScreen
        {
            get { return endScreen; }
        }        
        public GameObject EndScreenUI
        {
            get { return endScreenUI; }
        }
        public GameObject Shop
        {
            get { return shop; }
        }

        public CharacterController CharCont
        {
            get { return player.GetComponent<CharacterController>(); }
        }

        private void Start()
        {
            SetState(new Begin(this));
        }

        private void Update()
        {
            textUI.text = CharCont.CurrentGold.ToString();
            if (Input.GetKeyDown(KeyCode.Return))
            {
                OnPressStart();
            }
            if (Input.GetKeyDown(KeyCode.E))
            {
                OnPressBuy(5);
            }
            if (Input.GetKeyDown(KeyCode.C))
            {
                OnAdvanceTime();
            }
            if (CharCont.CurrentHealth <= 0)
            {
                OnPlayerDeath();
            }
        }

        public void OnPressStart()
        {
            StartCoroutine(State.StartGame());
        }
        public void OnPressBuy(int cost)
        {
            StartCoroutine(State.BuyPotion(cost));
        }
        public void OnAdvanceTime()
        {           
            StartCoroutine(State.AdvanceTime());
        }
        public void OnPlayerDeath()
        {
            StartCoroutine(State.PlayerDied());
        }
        public void OnSpawnTrolls(int amount)
        {
            for (int i = 0; i < amount; i++)
            {
                int pos = Random.Range(-79, 92);
                Instantiate(troll, new Vector3(pos, 0, 0), Quaternion.identity);
            }
        }
        public void OnNextPhase()
        {
            StartCoroutine(State.NextPhase());
        }
    }
}
