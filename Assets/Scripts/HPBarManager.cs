﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPBarManager : MonoBehaviour
{
    [SerializeField]
    private Sprite[] hpBars;

    private Image image;

    // Start is called before the first frame update
    void Awake()
    {
        image = GetComponent<Image>();
        image.sprite = hpBars[10];
    }

    public void ChangeBar(int index)
    {
        if (index < 0)
            return;
        image.sprite = hpBars[index];
    }
}
