﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{

    [SerializeField]
    private GameObject Player;

    private void Update()
    {
        Vector3 newPosition = new Vector3(Player.transform.position.x, transform.position.y, transform.position.z);
        transform.position = newPosition;
    }

}