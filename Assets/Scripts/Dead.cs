﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace createjam
{
    public class Dead : State
    {
        public Dead(TimeOfDaySystem todSys) : base(todSys)
        {
        }

        public override IEnumerator Start()
        {
            // Show Score
            yield return new WaitForSeconds(2);
            TodSys.EndScreen.SetActive(true);
            TodSys.EndScreenUI.SetActive(true);

            yield return 0;
        }

        public override IEnumerator StartGame()
        {
            TodSys.EndScreen.SetActive(false);
            TodSys.EndScreenUI.SetActive(false);

            TodSys.SetState(new Begin(TodSys));

            yield return 0;
        }
    }
}

