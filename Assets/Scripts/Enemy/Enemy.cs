﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    float moveSpeed;
    [SerializeField]
    Player player;
    [SerializeField]
    float detectDistance = 100;

    bool isChasing = false;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        if (player == null)
            player = FindObjectOfType<Player>();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (SpotPlayer())
        {
            isChasing = true;
        }

        if (isChasing)
        {
            transform.position = Vector2.MoveTowards(transform.position, player.transform.position, Time.deltaTime);
        }
    }

    protected bool SpotPlayer()
    {
        return Vector2.Distance(player.transform.position, transform.position) < 100;
    }
}
