﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace createjam
{
    public class Day : State
    {
        public Day(TimeOfDaySystem todSys) : base(todSys)
        {
        }

        public override IEnumerator Start()
        {
            Debug.Log("Good Morning, Goooooooood Morning");
            TodSys.LightTheme.SetActive(true);
            TodSys.DarkTheme.SetActive(false);

            TodSys.DayMusic.SetActive(true);
            TodSys.NightMusic.SetActive(false);

            TodSys.OnSpawnTrolls(TodSys.TrollsNextTurn);
            TodSys.TrollsNextTurn++;

            TodSys.Shop.SetActive(true);
            TodSys.OnNextPhase();
            TodSys.events.GetComponent<EventManager>().RunEvent();
            yield return 0;
        }

        public override IEnumerator BuyPotion(int cost)
        {
            if (TodSys.CharCont.CurrentGold >= cost)
            {
                TodSys.CharCont.CurrentGold -= cost;

                TodSys.CharCont.AttackDamage++;
            }
            else
            {
                // Play deny animation / sound
            }

            yield return 0;
        }


        public override IEnumerator PlayerDied()
        {
            // Play sad sound

            TodSys.SetState(new Dead(TodSys));

            yield return 0;
        }

        public override IEnumerator AdvanceTime()
        {
            // Play time sound

            TodSys.SetState(new Night(TodSys));

            yield return 0;
        }

        public override IEnumerator NextPhase()
        {
            yield return new WaitForSeconds(60); // Day Duration

            TodSys.SetState(new Night(TodSys));
            yield return 0;
        }
    }
}
