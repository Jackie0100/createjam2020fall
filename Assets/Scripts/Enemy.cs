﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    private Animator animator;
    [SerializeField]
    private LayerMask EnemyLayerMask;

    [SerializeField]
    private GameObject LootGold;
    [SerializeField]
    private int GoldOnDeath;
    [SerializeField]
    private int MaxHealth;
    [SerializeField]
    private int AttackDamage;
    [SerializeField]
    private float TimeBetweenAttacks;
    [SerializeField]
    private float AttackRange;

    [SerializeField]
    private float step;

    // Movement
    private Rigidbody2D rigidbody2D;
    private CapsuleCollider2D capsuleCollider2D;
    private Vector3 moveDir;

    // Combat
    private bool attacking;
    private int currentHealth;
    private GameObject player;
    // Environment
    private bool day = false;

    public int CurrentHealth
    {
        get { return currentHealth; }
        set
        {
            currentHealth = Mathf.Min(value, MaxHealth);
            if(currentHealth <= 0)
            {
                StartCoroutine("OnDeath");
            }
        }
    }

    private void Awake()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("Player");
        capsuleCollider2D = GetComponent<CapsuleCollider2D>();
        CurrentHealth = MaxHealth;
        animator.SetBool("TurnToStone", true);
    }

    public void TakeDamage(int damage)
    {
        if (day)
            return;
        CurrentHealth -= damage;
    }

    private void Update()
    {
        if (day)
        {
            StopCoroutine("ChasePlayer");
            return;
        }
        if (rigidbody2D.velocity.x == 0)
        {
            if (moveDir != Vector3.zero)
            {
                float angle = Mathf.Atan2(moveDir.y, moveDir.x) * Mathf.Rad2Deg;
                transform.localRotation = Quaternion.AngleAxis(angle, Vector3.up);
            }
            StopCoroutine("ChasePlayer");
            StartCoroutine("ChasePlayer", player.transform.position);
        }
        

        if (Vector3.Distance(transform.position, player.transform.position) < AttackRange && !attacking)
        {
            Debug.Log("In Range!");
            animator.SetTrigger("Attack");
            StartCoroutine("Attack");
        }
    }
    private IEnumerator ChasePlayer()
    {
        while (Vector3.Distance(transform.position, player.transform.position) > AttackRange && !attacking)
        {
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, step);

            yield return null;
        }
        yield return 0;
    }

    private IEnumerator Attack()
    {
        attacking = true;

        GameObject target = EnemiesInRange();
        if (target != null)
        {
            Debug.Log(string.Format("Attacking {0} for {1}", target.name, AttackDamage));
            target.GetComponent<CharacterController>().TakeDamage(AttackDamage);
        }

        yield return new WaitForSeconds(TimeBetweenAttacks);

        attacking = false;
    }

    private GameObject EnemiesInRange()
    {
        RaycastHit2D raycastHit2D = Physics2D.BoxCast(capsuleCollider2D.bounds.center, capsuleCollider2D.bounds.size, 0f, transform.right, AttackRange, EnemyLayerMask);
        if (raycastHit2D.collider != null)
            return raycastHit2D.collider.gameObject;
        return null;
    }


    private IEnumerator OnDeath()
    {
        // Play Death animation

        yield return new WaitForSeconds(1);

        // Drop Loot
        Instantiate(LootGold, transform.position, Quaternion.identity);
        // Disapear
        Destroy(this.gameObject);
    }
    public void KillSwitch()
    {
        Destroy(this.gameObject);
    }

    void OnEnable()
    {
        EventManager.OnClicked += Day;
    }

    void OnDisable()
    {
        EventManager.OnClicked -= Day;
    }

    void Day()
    {
        day = !day;
        animator.SetBool("TurnToStone", day);
    }
}
