﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace createjam
{
    public class Night : State
    {
        public Night(TimeOfDaySystem todSys) : base(todSys)
        {
        }

        public override IEnumerator Start()
        {
            Debug.Log("Spooky Scary Skeletons!");
            TodSys.LightTheme.SetActive(false);
            TodSys.DarkTheme.SetActive(true);

            TodSys.DayMusic.SetActive(false);
            TodSys.NightMusic.SetActive(true);

            TodSys.Shop.SetActive(false);
            TodSys.OnNextPhase();
            TodSys.events.GetComponent<EventManager>().RunEvent();

            yield return 0;
        }

        public override IEnumerator PlayerDied()
        {
            // Play sad sound

            TodSys.SetState(new Dead(TodSys));

            yield return 0;
        }

        public override IEnumerator AdvanceTime()
        {
            // Play time sound

            TodSys.SetState(new Day(TodSys));

            yield return 0;
        }

        public override IEnumerator NextPhase()
        {
            yield return new WaitForSeconds(120); // Night Duration

            TodSys.SetState(new Day(TodSys));
            yield return 0;
        }
    }
}
