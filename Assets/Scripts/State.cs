﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace createjam
{
    public class State
    {
        protected TimeOfDaySystem TodSys;

        public State(TimeOfDaySystem todSys)
        {
            TodSys = todSys;
        }

        public virtual IEnumerator Start()
        {
            yield break;
        }

        public virtual IEnumerator StartGame()
        {
            yield break;
        }

        public virtual IEnumerator BuyPotion( int cost)
        {
            yield break;
        }

        public virtual IEnumerator AdvanceTime()
        {
            yield break;
        }
        public virtual IEnumerator NextPhase()
        {
            yield break;
        }


        public virtual IEnumerator PlayerDied()
        {
            yield break;
        }
    }
}
