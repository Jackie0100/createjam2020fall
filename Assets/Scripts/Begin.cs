﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace createjam
{
    public class Begin : State
    {
        public Begin(TimeOfDaySystem todSys) : base(todSys)
        {
        }

        public override IEnumerator Start()
        {
            Debug.Log("Game Starting, Strap In!!!");

            TodSys.StartScreen.SetActive(true);
            TodSys.StartScreenUI.SetActive(true);

            yield return 0;
            
        }

        public override IEnumerator StartGame()
        {
            TodSys.score = 0;
            TodSys.CharCont.FullHealth();
            TodSys.CharCont.CurrentGold = 0;

            GameObject[] badGuys= GameObject.FindGameObjectsWithTag("Enemy");

            foreach(GameObject go in badGuys)
            {
                go.GetComponent<Enemy>().KillSwitch();
            }

            TodSys.StartScreen.SetActive(false);
            TodSys.StartScreenUI.SetActive(false);

            TodSys.SetState(new Day(TodSys));

            yield return 0;
        }
    }
}
