﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lunaris.ScriptableVariables
{
    [CreateAssetMenu(menuName = "ScriptableObjects/GameObjectCollection")]
    public class GameObjectCollection : ObservableCollectionRef<GameObject>
    {
    }
}