﻿using UnityEngine;

namespace Lunaris.ScriptableVariables
{
    [CreateAssetMenu(menuName = "Lunaris/Variable/BoolRef")]
    public class BoolRef : ValueRef<bool>
    {
    }
}