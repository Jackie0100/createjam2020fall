﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "ScriptableObjects/FloatRef")]
public class FloatRef : ScriptableObject
{
    [SerializeField]
    protected float value;
    [SerializeField]
    public event FloatRefValueChanged OnValueChanged;
    [SerializeField]
    public UnityEvent<float> _event;

    public virtual float Value
    {
        get
        {
            return value;
        }
        set
        {
            this.value = value;
            OnValueChanged?.Invoke(value);
        }
    }

    public virtual void Add(float val)
    {
        Value += val;
    }
}

public delegate void FloatRefValueChanged(float value);