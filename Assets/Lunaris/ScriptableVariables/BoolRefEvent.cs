﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lunaris.ScriptableVariables
{
    [CreateAssetMenu(menuName = "ScriptableObjects/BoolRefEvent")]
    public class BoolRefEvent : BoolRef
    {
        [SerializeField]
        BoolGameEvent _onValueChangedEvent;
        [SerializeField]
        public event BoolRefValueChanged OnValueChanged;

        public override bool Value
        {
            get
            {
                return base.Value;
            }
            set
            {
                base.Value = value;
                OnValueChanged?.Invoke(value);
                _onValueChangedEvent?.Invoke(value);
            }
        }
    }

    public delegate void BoolRefValueChanged(bool value);
}