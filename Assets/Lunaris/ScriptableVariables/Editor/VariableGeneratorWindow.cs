﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Lunaris.ScriptableVariables
{
    public class VariableGeneratorWindow : EditorWindow
    {
        static string _path = "";
        static List<Type> _selectedTypes;
        static List<Type> _searchResultSelectedTypes;
        static List<Type> _foundTypes;
        static List<Type> _searchTypes;
        static VariableGeneratorWindow window;
        string searchString = "";
        Vector2 left = Vector2.zero;
        Vector2 right = Vector2.zero;

        [MenuItem("Lunaris/Scriptable Variables Code Generator")]
        static void Init()
        {
            window = ScriptableObject.CreateInstance<VariableGeneratorWindow>();
            _selectedTypes = new List<Type>();
            _searchResultSelectedTypes = new List<Type>();
            _foundTypes = new List<Type>();
            _searchTypes = new List<Type>();

            window.minSize = new Vector2(400, 600);
            foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (Type t in a.GetTypes().Where(x => x.GetCustomAttribute<SerializableAttribute>() != null || x.IsPrimitive || (x.IsValueType && x.IsPublic) || (x.IsSubclassOf(typeof(UnityEngine.Object)) && x.IsPublic)))
                {
                    _foundTypes.Add(t);
                }
            }
            window.Show();
            if (string.IsNullOrWhiteSpace(_path))
            {
                _path = Application.dataPath;
            }
        }

        private void OnGUI()
        {
            Rect rect = window.position;

            if (!Directory.Exists(_path))
            {
                EditorGUILayout.HelpBox(_path + " isn't a valid directory!", MessageType.Error);
            }
            if (GUILayout.Button("Select Folder: " + _path))
            {
                _path = EditorUtility.OpenFolderPanel("Select Target Folder", "", "");
            }

            searchString = GUILayout.TextField(searchString, GUI.skin.FindStyle("ToolbarSeachTextField"));

            _searchResultSelectedTypes = _selectedTypes.Where(t => t.Name.ToLower().Contains(searchString.ToLower())).ToList();
            _searchTypes = _foundTypes.Where(t => t.Name.ToLower().Contains(searchString.ToLower())).ToList();

            EditorGUILayout.BeginHorizontal();
            GUILayout.BeginArea(new Rect(0, 50, rect.width / 2, rect.height - 80));

            EditorGUILayout.LabelField("Found Types");
            left = EditorGUILayout.BeginScrollView(left);
            foreach (var t in _searchTypes)
            {
                if (GUILayout.Button(t.FullName))
                {
                    _foundTypes.Remove(t);
                    _selectedTypes.Add(t);
                }
            }
            EditorGUILayout.EndScrollView();
            GUILayout.EndArea();

            GUILayout.BeginArea(new Rect(rect.width / 2, 50, rect.width / 2, rect.height - 80));

            EditorGUILayout.LabelField("Selected Types");
            right = EditorGUILayout.BeginScrollView(right);
            foreach (var t in _searchResultSelectedTypes)
            {
                if (GUILayout.Button(t.FullName))
                {
                    _foundTypes.Add(t);
                    _selectedTypes.Remove(t);
                }
            }
            EditorGUILayout.EndScrollView();
            GUILayout.EndArea();
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space(rect.height - 66);

            if (GUILayout.Button("Generate " + _selectedTypes.Count + " File(s)"))
            {
                GenerateFiles();
                AssetDatabase.Refresh();
                window.Close();
            }
        }

        private void GenerateFiles()
        {
            if (!Directory.Exists(_path))
            {
                Debug.Log(_path + " isn't a valid directory!");
                throw new Exception(_path + " isn't a valid directory!");
            }

            foreach (var t in _selectedTypes)
            {
                string typename = "";

                if (t == typeof(bool))
                {
                    typename = "Bool";
                }
                else if (t == typeof(byte))
                {
                    typename = "Byte";
                }
                else if (t == typeof(sbyte))
                {
                    typename = "Sbyte";
                }
                else if (t == typeof(short))
                {
                    typename = "Short";
                }
                else if (t == typeof(ushort))
                {
                    typename = "Ushort";
                }
                else if (t == typeof(int))
                {
                    typename = "Int";
                }
                else if (t == typeof(uint))
                {
                    typename = "Uint";
                }
                else if (t == typeof(long))
                {
                    typename = "Long";
                }
                else if (t == typeof(ulong))
                {
                    typename = "Ulong";
                }
                else if (t == typeof(double))
                {
                    typename = "Double";
                }
                else if (t == typeof(float))
                {
                    typename = "Float";
                }
                else if (t == typeof(decimal))
                {
                    typename = "Decimal";
                }
                else if (t == typeof(string))
                {
                    typename = "String";
                }
                else if (t == typeof(char))
                {
                    typename = "Char";
                }
                else
                {
                    typename = t.Name;
                }

                if (File.Exists(_path + "\\" + typename + "Ref.cs"))
                {
                    continue;
                }

                using (StreamWriter sw = new StreamWriter(_path + "\\" + typename + "Ref.cs"))
                {

                    sw.WriteLine("using UnityEngine;");
                    if (typename == t.Name && t.Namespace != "UnityEngine")
                    {
                        //sw.WriteLine("using " + t.Namespace + ";");
                    }
                    sw.WriteLine("namespace Lunaris.ScriptableVariables\n{\n");
                    sw.WriteLine("\t[CreateAssetMenu(menuName = \"Lunaris/Variables/" + typename + "Ref\")]");
                    if (typename == t.Name)
                    {
                        sw.WriteLine("\tpublic class " + typename + "Ref : ValueRef<" + t.FullName + "> { }\n}");
                    }
                    else
                    {
                        sw.WriteLine("\tpublic class " + typename + "Ref : ValueRef<" + typename.ToLower() + "> { }\n}");
                    }
                    sw.Flush();
                }
            }
        }
    }
}