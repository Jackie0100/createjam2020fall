﻿using UnityEngine;
using UnityEngine.Events;

public class BoolGameEventListener : GameEventListener<bool>
{
    [SerializeField] private BoolGameEvent _gameEvent;
    [SerializeField] private BoolUnityEvent _response;

    public override GameEvent<bool> GameEvent
    {
        get
        {
            return _gameEvent;
        }
    }

    public override UnityEvent<bool> Response
    {
        get
        {
            return _response;
        }
    }
}