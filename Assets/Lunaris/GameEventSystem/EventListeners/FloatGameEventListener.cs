﻿using UnityEngine;
using UnityEngine.Events;

public class FloatGameEventListener : GameEventListener<float>
{
    [SerializeField] private FloatGameEvent _gameEvent;
    [SerializeField] private FloatUnityEvent _response;

    public override GameEvent<float> GameEvent
    {
        get
        {
            return _gameEvent;
        }
    }

    public override UnityEvent<float> Response
    {
        get
        {
            return _response;
        }
    }
}
