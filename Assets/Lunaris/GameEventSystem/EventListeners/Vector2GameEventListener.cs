﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Vector2GameEventListener : GameEventListener<Vector2>
{
    [SerializeField] private Vector2GameEvent _gameEvent;
    [SerializeField] private Vector2UnityEvent _response;

    public override GameEvent<Vector2> GameEvent
    {
        get
        {
            return _gameEvent;
        }
    }

    public override UnityEvent<Vector2> Response
    {
        get
        {
            return _response;
        }
    }
}
