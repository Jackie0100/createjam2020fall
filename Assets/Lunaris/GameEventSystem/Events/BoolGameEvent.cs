﻿using UnityEngine;

[CreateAssetMenu(menuName = "Events/BoolGameEvent")]
public class BoolGameEvent : GameEvent<bool>
{
}