﻿using Lunaris.Attributes;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu( menuName = "Events/GameEvent" )]
public class GameEvent : ScriptableObject, IGameEvent
{
    private List<IGameEventListener> _listeners = new List<IGameEventListener>();
    [Button]
    public void Invoke()
    {
        for (int i = _listeners.Count - 1; i >= 0; i--)
        {
            _listeners[i].OnEventRaised(); 
        }
    }

    public void RegisterListener(IGameEventListener listener)
    {
        _listeners.Add(listener);
    }

    public void UnregisterListener(IGameEventListener listener)
    {
        _listeners.Remove(listener);
    }
}

public abstract class GameEvent<T> : GameEvent, IGameEvent<T>
{
    private List<IGameEventListener<T>> _listeners = new List<IGameEventListener<T>>();

    public void Invoke(T value)
    {
        for (int i = _listeners.Count - 1; i >= 0; i--)
        {
            _listeners[i].OnEventRaised(value);
        }
    }

    public void RegisterListener(IGameEventListener<T> listener)
    {
        _listeners.Add(listener);
    }

    public void UnregisterListener(IGameEventListener<T> listener)
    {
        _listeners.Remove(listener);
    }
}