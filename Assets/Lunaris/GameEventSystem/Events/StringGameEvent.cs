﻿using UnityEngine;

[CreateAssetMenu(menuName = "Events/StringGameEvent")]
public class StringGameEvent : GameEvent<string>
{
}