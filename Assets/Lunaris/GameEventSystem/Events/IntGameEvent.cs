﻿using UnityEngine;

[CreateAssetMenu(menuName = "Events/IntGameEvent")]
public class IntGameEvent : GameEvent<int>
{
}