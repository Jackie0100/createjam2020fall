﻿using UnityEngine;

[CreateAssetMenu(menuName = "Events/Vector3GameEvent")]
public class Vector3GameEvent : GameEvent<Vector3>
{
}