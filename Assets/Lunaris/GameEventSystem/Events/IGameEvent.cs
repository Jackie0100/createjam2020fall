﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGameEvent
{
    void Invoke();
    void RegisterListener(IGameEventListener listener);
    void UnregisterListener(IGameEventListener listener);
}

public interface IGameEvent<T>
{
    void Invoke(T value);
    void RegisterListener(IGameEventListener<T> listener);
    void UnregisterListener(IGameEventListener<T> listener);
}
