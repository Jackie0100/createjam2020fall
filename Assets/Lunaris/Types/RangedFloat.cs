﻿using System;
using UnityEngine;

[Serializable]
public struct RangedFloat
{
    [SerializeField]
    private float minValue;
    [SerializeField]
    private float maxValue;

    public float MinValue
    {
        get
        {
            return minValue;
        }

        set
        {
            minValue = value;
        }
    }

    public float MaxValue
    {
        get
        {
            return maxValue;
        }

        set
        {
            maxValue = value;
        }
    }
}