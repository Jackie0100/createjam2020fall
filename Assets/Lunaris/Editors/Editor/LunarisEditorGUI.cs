﻿using Lunaris.Attributes;
using Lunaris.Editors.Windows;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Lunaris.Editors
{
    internal static class LunarisEditorGUI
    {
        static GUIStyle _cardHeader;
        static GUIStyle _cardBody;

        static LunarisEditorGUI()
        {
            _cardHeader = new GUIStyle();
            _cardHeader.border = new RectOffset(1, 1, 1, 1);
            _cardHeader.alignment = TextAnchor.MiddleLeft;
            _cardHeader.normal.textColor = LunarisEditorColors.Text;
            //_cardHeader.normal.background = new Texture2D(1, 1);
            //_cardHeader.normal.background.SetPixels(new Color[] { LunarisEditorColors.CardHeader });

            //_cardBody = EditorStyles.helpBox;

            //_cardBody.normal.background = new Texture2D(1, 1);
            //_cardBody.normal.background.SetPixels(new Color[] { LunarisEditorColors.CardBody });

        }

        public static void PropertyFieldLayout()
        {

        }

        public static void Seperator()
        {
            EditorGUILayout.Space(4);
            var rect = GUILayoutUtility.GetLastRect();
            rect.y += rect.height;
            rect.height = 1;
            EditorGUI.DrawRect(rect, LunarisEditorColors.Seperator);
        }

        public static void BeginButtonCard(string title)
        {
            var tempcol = GUI.backgroundColor;
            EditorGUILayout.Space(4);
            Rect rect;


            //GUI.Box(rect, title, _card);


            //GUI.backgroundColor = LunarisEditorColors.CardHeader;

            //GUIStyle s = EditorStyles.helpBox;
            //s.padding = new RectOffset();
            //s.margin = new RectOffset();


            //GUI.backgroundColor = LunarisEditorColors.CardHeader;
            //rect = GUILayoutUtility.GetLastRect();
            //rect.height = 20;
            GUIStyle header = GUI.skin.FindStyle("CN Box");
            header.padding = new RectOffset();
            header.margin = new RectOffset();
            EditorGUILayout.BeginHorizontal(header);

            //DrawBorderedRect(rect, LunarisEditorColors.CardBorder, LunarisEditorColors.CardHeader);
            EditorGUILayout.LabelField(title);
            GUILayout.Button("Invoke");

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space(-5);

            EditorGUILayout.BeginVertical(GUI.skin.FindStyle("FrameBox"));
        }

        public static void EndButtonCard()
        {
            EditorGUILayout.EndVertical();
        }

        public static void ListHeader(SerializedProperty property)
        {
            EditorGUILayout.BeginHorizontal(LunarisEditorStyles.ToolBarHeader);

            property.isExpanded = GUILayout.Toggle(property.isExpanded, property.displayName, LunarisEditorStyles.FoldoutStyle, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));

            GUILayout.FlexibleSpace();
            if (!LunarisEditorSettingsWindow.Settings.HideElementCountField)
            {
                GUIStyle sizefieldStyle = new GUIStyle("ToolbarTextField");
                sizefieldStyle.alignment = TextAnchor.MiddleRight;
                Debug.Log(GUI.GetNameOfFocusedControl());
                GUI.SetNextControlName("ArraySizeField");
                int newArraySize = EditorGUILayout.IntField(property.arraySize, sizefieldStyle, GUILayout.ExpandWidth(false), GUILayout.MaxWidth(48), GUILayout.MinWidth(48), GUILayout.ExpandHeight(false));
                if (newArraySize != property.arraySize)
                    property.arraySize = newArraySize;
            }

            if (GUILayout.Button("+", LunarisEditorStyles.ToolBarButton, GUILayout.ExpandWidth(false), GUILayout.ExpandHeight(true)))
            {
                property.arraySize++;
            }

            EditorGUILayout.EndHorizontal();
        }

        private delegate void PropertyFieldFunction(Rect rect, SerializedProperty property, GUIContent label, bool includeChildren);

        public static void PropertyField(Rect rect, SerializedProperty property, bool includeChildren)
        {
            PropertyField_Implementation(rect, property, includeChildren, DrawPropertyField);
        }

        public static void PropertyField_Layout(SerializedProperty property, bool includeChildren)
        {
            Rect dummyRect = new Rect();
            PropertyField_Implementation(dummyRect, property, includeChildren, DrawPropertyField_Layout);
        }

        private static void DrawPropertyField(Rect rect, SerializedProperty property, GUIContent label, bool includeChildren)
        {
            EditorGUI.PropertyField(rect, property, label, includeChildren);
        }

        private static void DrawPropertyField_Layout(Rect rect, SerializedProperty property, GUIContent label, bool includeChildren)
        {
            EditorGUILayout.PropertyField(property, label, includeChildren);
        }

        private static void PropertyField_Implementation(Rect rect, SerializedProperty property, bool includeChildren, PropertyFieldFunction propertyFieldFunction)
        {
            SpecialCaseDrawerAttribute specialCaseAttribute = PropertyUtility.GetAttribute<SpecialCaseDrawerAttribute>(property);
            if (specialCaseAttribute != null)
            {
                //specialCaseAttribute.GetDrawer().OnGUI(rect, property);
            }
            else
            {
                GUIContent label = new GUIContent(PropertyUtility.GetLabel(property));
                bool anyDrawerAttribute = PropertyUtility.GetAttributes<DrawerAttribute>(property).Any();

                if (!anyDrawerAttribute)
                {
                    // Drawer attributes check for visibility, enableability and validator themselves,
                    // so if a property doesn't have a DrawerAttribute we need to check for these explicitly

                    // Check if visible
                    //bool visible = PropertyUtility.IsVisible(property);
                    //if (!visible)
                    //{
                    //    return;
                    //}

                    // Validate
                    ValidatorAttribute[] validatorAttributes = PropertyUtility.GetAttributes<ValidatorAttribute>(property);
                    //foreach (var validatorAttribute in validatorAttributes)
                    //{
                    //    validatorAttribute.GetValidator().ValidateProperty(property);
                    //}

                    // Check if enabled and draw
                    EditorGUI.BeginChangeCheck();
                    //bool enabled = PropertyUtility.IsEnabled(property);

                    //using (new EditorGUI.DisabledScope(disabled: !enabled))
                    //{
                    //    propertyFieldFunction.Invoke(rect, property, label, includeChildren);
                    //}

                    // Call OnValueChanged callbacks
                    if (EditorGUI.EndChangeCheck())
                    {
                        //PropertyUtility.CallOnValueChangedCallbacks(property);
                    }
                }
                else
                {
                    // We don't need to check for enableIfAttribute
                    propertyFieldFunction.Invoke(rect, property, label, includeChildren);
                }
            }
        }

        public static float GetIndentLength(Rect sourceRect)
        {
            Rect indentRect = EditorGUI.IndentedRect(sourceRect);
            float indentLength = indentRect.x - sourceRect.x;

            return indentLength;
        }

        public static void BeginBoxGroup_Layout(string label = "")
        {
            EditorGUILayout.BeginVertical(GUI.skin.box);
            if (!string.IsNullOrEmpty(label))
            {
                EditorGUILayout.LabelField(label, EditorStyles.boldLabel);
            }
        }

        public static void EndBoxGroup_Layout()
        {
            EditorGUILayout.EndVertical();
        }

        public static bool BeginFoldout_Layout(bool unfolded, string label = "")
        {
            return EditorGUILayout.BeginFoldoutHeaderGroup(unfolded, label);
        }

        public static void EndFoldout_Layout()
        {
            EditorGUILayout.EndFoldoutHeaderGroup();
        }

        static void DrawBorderedRect(Rect rect, Color background, Color foreground)
        {
            EditorGUI.DrawRect(rect, background);
            rect.width -= 2;
            rect.height -= 2;
            rect.x += 1;
            rect.y += 1;
            EditorGUI.DrawRect(rect, foreground);
        }

        //public static void NativeProperty_Layout(UnityEngine.Object target, PropertyInfo property)
        //{
        //    object value = property.GetValue(target, null);

        //    if (value == null)
        //    {
        //        string warning = string.Format("{0} is null. {1} doesn't support reference types with null value", property.Name, typeof(ShowNativePropertyAttribute).Name);
        //        HelpBox_Layout(warning, MessageType.Warning, context: target);
        //    }
        //    else if (!Field_Layout(value, property.Name))
        //    {
        //        string warning = string.Format("{0} doesn't support {1} types", typeof(ShowNativePropertyAttribute).Name, property.PropertyType.Name);
        //        HelpBox_Layout(warning, MessageType.Warning, context: target);
        //    }
        //}

        //public static void NonSerializedField_Layout(UnityEngine.Object target, FieldInfo field)
        //{
        //    object value = field.GetValue(target);

        //    if (value == null)
        //    {
        //        string warning = string.Format("{0} is null. {1} doesn't support reference types with null value", field.Name, typeof(ShowNonSerializedFieldAttribute).Name);
        //        HelpBox_Layout(warning, MessageType.Warning, context: target);
        //    }
        //    else if (!Field_Layout(value, field.Name))
        //    {
        //        string warning = string.Format("{0} doesn't support {1} types", typeof(ShowNonSerializedFieldAttribute).Name, field.FieldType.Name);
        //        HelpBox_Layout(warning, MessageType.Warning, context: target);
        //    }
        //}

        public static void HorizontalLine(Rect rect, float height, Color color)
        {
            rect.height = height;
            EditorGUI.DrawRect(rect, color);
        }

        public static void HelpBox(Rect rect, string message, MessageType type, UnityEngine.Object context = null, bool logToConsole = false)
        {
            EditorGUI.HelpBox(rect, message, type);

            if (logToConsole)
            {
                DebugLogMessage(message, type, context);
            }
        }

        public static void HelpBox_Layout(string message, MessageType type, UnityEngine.Object context = null, bool logToConsole = false)
        {
            EditorGUILayout.HelpBox(message, type);

            if (logToConsole)
            {
                DebugLogMessage(message, type, context);
            }
        }

        private static void DebugLogMessage(string message, MessageType type, UnityEngine.Object context)
        {
            switch (type)
            {
                case MessageType.None:
                case MessageType.Info:
                    Debug.Log(message, context);
                    break;
                case MessageType.Warning:
                    Debug.LogWarning(message, context);
                    break;
                case MessageType.Error:
                    Debug.LogError(message, context);
                    break;
            }
        }
    }

    internal static class LunarisEditorColors
    {
        public static Color EditorBackground
        {
            get
            {
                if (EditorGUIUtility.isProSkin)
                {
                    return D_EditorBackground;
                }
                else
                {
                    return L_EditorBackground;
                }
            }
        }

        public static Color InspectorHeader
        {
            get
            {
                if (EditorGUIUtility.isProSkin)
                {
                    return D_InspectorHeader;
                }
                else
                {
                    return L_InspectorHeader;
                }
            }
        }

        public static Color InspectorHeaderBackground
        {
            get
            {
                if (EditorGUIUtility.isProSkin)
                {
                    return D_InspectorHeaderBackground;
                }
                else
                {
                    return L_InspectorHeaderBackground;
                }
            }
        }

        public static Color InspectorBackground
        {
            get
            {
                if (EditorGUIUtility.isProSkin)
                {
                    return D_InspectorBackground;
                }
                else
                {
                    return L_InspectorBackground;
                }
            }
        }

        public static Color InspectorDarkBackground
        {
            get
            {
                if (EditorGUIUtility.isProSkin)
                {
                    return D_InspectorDarkBackground;
                }
                else
                {
                    return L_InspectorDarkBackground;
                }
            }
        }

        public static Color Seperator
        {
            get
            {
                if (EditorGUIUtility.isProSkin)
                {
                    return D_Seperator;
                }
                else
                {
                    return L_Seperator;
                }
            }
        }

        public static Color CardHeader
        {
            get
            {
                if (EditorGUIUtility.isProSkin)
                {
                    return D_CardHeader;
                }
                else
                {
                    return L_CardHeader;
                }
            }
        }

        public static Color CardBody
        {
            get
            {
                if (EditorGUIUtility.isProSkin)
                {
                    return D_CardBody;
                }
                else
                {
                    return L_CardBody;
                }
            }
        }

        public static Color CardBorder
        {
            get
            {
                if (EditorGUIUtility.isProSkin)
                {
                    return D_CardBorder;
                }
                else
                {
                    return L_CardBorder;
                }
            }
        }

        public static Color Text
        {
            get
            {
                if (EditorGUIUtility.isProSkin)
                {
                    return D_Text;
                }
                else
                {
                    return L_Text;
                }
            }
        }

        public static Color SelectedText
        {
            get
            {
                if (EditorGUIUtility.isProSkin)
                {
                    return D_SelectedText;
                }
                else
                {
                    return L_SelectedText;
                }
            }
        }

        public static Color SelectedBackground
        {
            get
            {
                if (EditorGUIUtility.isProSkin)
                {
                    return D_SelectedBackground;
                }
                else
                {
                    return L_SelectedBackground;
                }
            }
        }

        public static Color SelectedBackgroundFocused
        {
            get
            {
                if (EditorGUIUtility.isProSkin)
                {
                    return D_SelectedBackgroundFocused;
                }
                else
                {
                    return L_SelectedBackgroundFocused;
                }
            }
        }

        public static Color SelectedBackgroundUnFocused
        {
            get
            {
                if (EditorGUIUtility.isProSkin)
                {
                    return D_SelectedBackgroundUnFocused;
                }
                else
                {
                    return L_SelectedBackgroundUnFocused;
                }
            }
        }

        public static Texture2D EnumPickerBackgroundTexture
        {
            get
            {
                if (EditorGUIUtility.isProSkin)
                {
                    return D_EnumPickerBackgroundTexture;
                }
                else
                {
                    return L_EnumPickerBackgroundTexture;
                }
            }
        }

        public static Texture2D EnumPickerHoverTexture
        {
            get
            {
                if (EditorGUIUtility.isProSkin)
                {
                    return D_EnumPickerHoverTexture;
                }
                else
                {
                    return L_EnumPickerHoverTexture;
                }
            }
        }

        public static Texture2D EnumPickerSelectedTexture
        {
            get
            {
                if (EditorGUIUtility.isProSkin)
                {
                    return D_EnumPickerSelectedTexture;
                }
                else
                {
                    return L_EnumPickerSelectedTexture;
                }
            }
        }

        public static Texture2D Transparent
        {
            get
            {
                return TransparentTex;
            }
        }

        public static Texture2D Black
        {
            get
            {
                return BlackTex;
            }
        }


        static readonly Color32 L_EditorBackground = new Color32(138, 138, 138, 255);

        static readonly Color32 L_InspectorHeader = new Color32(203, 203, 203, 255);
        static readonly Color32 L_InspectorHeaderBackground = new Color32(165, 165, 165, 255);
        static readonly Color32 L_InspectorBackground = new Color32(194, 194, 194, 255);
        static readonly Color32 L_InspectorDarkBackground = new Color32(170, 170, 170, 255);

        static readonly Color32 L_Seperator = new Color32(153, 153, 153, 255);

        static readonly Color32 L_CardHeader = new Color32(182, 182, 182, 255);
        static readonly Color32 L_CardBody = new Color32(161, 161, 161, 255);
        static readonly Color32 L_CardBorder = new Color32(170, 170, 170, 255);

        static readonly Color32 L_Text = new Color32(0, 0, 0, 255);
        static readonly Color32 L_SelectedText = new Color32(255, 255, 255, 255);

        static readonly Color32 L_SelectedBackground = new Color32(58, 114, 176, 255);
        static readonly Color32 L_SelectedBackgroundFocused = new Color32(58, 114, 176, 255);
        static readonly Color32 L_SelectedBackgroundUnFocused = new Color32(174, 174, 174, 255);


        static readonly Color32 D_EditorBackground = new Color32(25, 25, 25, 255);

        static readonly Color32 D_InspectorHeader = new Color32(60, 60, 60, 255);
        static readonly Color32 D_InspectorHeaderBackground = new Color32(40, 40, 40, 255);
        static readonly Color32 D_InspectorBackground = new Color32(56, 56, 56, 255);
        static readonly Color32 D_InspectorDarkBackground = new Color32(45, 45, 45, 255);

        static readonly Color32 D_Seperator = new Color32(35, 35, 35, 255);

        static readonly Color32 D_CardHeader = new Color32(53, 53, 53, 255);
        static readonly Color32 D_CardBody = new Color32(65, 65, 65, 255);
        static readonly Color32 D_CardBorder = new Color32(36, 36, 36, 255);

        static readonly Color32 D_Text = new Color32(200, 200, 200, 255);
        static readonly Color32 D_SelectedText = new Color32(200, 200, 200, 255);

        static readonly Color32 D_SelectedBackground = new Color32(44, 93, 135, 255);
        static readonly Color32 D_SelectedBackgroundFocused = new Color32(44, 93, 135, 255);
        static readonly Color32 D_SelectedBackgroundUnFocused = new Color32(77, 77, 77, 255);

        static readonly Texture2D L_EnumPickerBackgroundTexture = new Texture2D(1, 1);
        static readonly Texture2D D_EnumPickerBackgroundTexture = new Texture2D(1, 1);
        static readonly Texture2D L_EnumPickerHoverTexture = new Texture2D(1, 1);
        static readonly Texture2D D_EnumPickerHoverTexture = new Texture2D(1, 1);
        static readonly Texture2D L_EnumPickerSelectedTexture = new Texture2D(1, 1);
        static readonly Texture2D D_EnumPickerSelectedTexture = new Texture2D(1, 1);

        static readonly Texture2D TransparentTex = new Texture2D(1, 1);
        static readonly Texture2D BlackTex = new Texture2D(1, 1);



        static LunarisEditorColors()
        {
            L_EnumPickerBackgroundTexture.LoadImage(Convert.FromBase64String("iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mP8/x8AAwMCAO+ip1sAAAAASUVORK5CYII="));
            D_EnumPickerBackgroundTexture.LoadImage(Convert.FromBase64String("iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mO0+Q8AAX0BPfL/26EAAAAASUVORK5CYII="));
            L_EnumPickerHoverTexture.LoadImage(Convert.FromBase64String("iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNcuFDgPwAFkAJTJBoCQwAAAABJRU5ErkJggg=="));
            D_EnumPickerHoverTexture.LoadImage(Convert.FromBase64String("iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mN0/A8AAYcBQqKIKZoAAAAASUVORK5CYII="));
            L_EnumPickerSelectedTexture.LoadImage(Convert.FromBase64String("iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mO0KtrwHwAEpwJdOOWc4wAAAABJRU5ErkJggg=="));
            D_EnumPickerSelectedTexture.LoadImage(Convert.FromBase64String("iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mPUiW3/DwAD3gIRpBvtqwAAAABJRU5ErkJggg=="));
            TransparentTex.LoadImage(Convert.FromBase64String("iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="));
            BlackTex.LoadImage(Convert.FromBase64String("iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII="));
        }
    }
}