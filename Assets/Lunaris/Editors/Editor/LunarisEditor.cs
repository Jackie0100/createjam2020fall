﻿using UnityEngine;
using UnityEditor;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Reflection.Emit;
using UnityEditorInternal;
using Lunaris.Attributes;
using Lunaris;
using Lunaris.Editors.Windows;

namespace Lunaris.Editors
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(UnityEngine.Object), true, isFallback = true)]
    public class LunarisEditor : UnityEditor.Editor
    {
        protected bool isInitialized = false;
        protected bool hasEditable = false;
        public bool isSubEditor;
        private readonly GUILayoutOption uiExpandWidth = GUILayout.ExpandWidth(true);
        private readonly GUILayoutOption uiWidth50 = GUILayout.Width(50);
        private readonly GUIContent labelBtnCreate = new GUIContent("Create");
        private GUIStyle styleEditBox;

        protected bool hideScript = false;
        protected Dictionary<string, ScriptableObject> buttonParameterObjects;

        private readonly List<SortableListData> listIndex = new List<SortableListData>();
        private readonly Dictionary<string, Editor> editableIndex = new Dictionary<string, Editor>();
        private List<SerializedProperty> _serializedProperties = new List<SerializedProperty>();

        Texture2D transparent;


        protected struct ContextMenuData
        {
            public string menuItem;
            public MethodInfo function;
            public MethodInfo validate;

            public ContextMenuData(string item)
            {
                menuItem = item;
                function = null;
                validate = null;
            }
        }

        protected Dictionary<string, ContextMenuData> contextData = new Dictionary<string, ContextMenuData>();

        private void OnEnable()
        {
            buttonParameterObjects = new Dictionary<string, ScriptableObject>();

            InitInspector(true);
            hideScript = ShouldHideScriptField();
        }

        private void Awake()
        {
        }

        protected virtual void InitInspector(bool force)
        {
            if (force)
                isInitialized = false;
            InitInspector();
        }

        protected virtual void InitInspector()
        {
            if (isInitialized)
                return;

            InitMethodParameterObjects();
            FindTargetProperties();
            FindContextMenu();
        }

        private void FindContextMenu()
        {
            contextData.Clear();

            // Get context menu
            Type targetType = target.GetType();
            Type contextMenuType = typeof(ContextMenu);
            MethodInfo[] methods = GetAllMethods(targetType).ToArray();
            for (int index = 0; index < methods.GetLength(0); ++index)
            {
                MethodInfo methodInfo = methods[index];
                foreach (ContextMenu contextMenu in methodInfo.GetCustomAttributes(contextMenuType, false))
                {
                    if (contextData.ContainsKey(contextMenu.menuItem))
                    {
                        var data = contextData[contextMenu.menuItem];
                        if (contextMenu.validate)
                            data.validate = methodInfo;
                        else
                            data.function = methodInfo;
                        contextData[data.menuItem] = data;
                    }
                    else
                    {
                        var data = new ContextMenuData(contextMenu.menuItem);
                        if (contextMenu.validate)
                            data.validate = methodInfo;
                        else
                            data.function = methodInfo;
                        contextData.Add(data.menuItem, data);
                    }
                }
            }
        }

        private IEnumerable<MethodInfo> GetAllMethods(Type t)
        {
            if (t == null)
                return Enumerable.Empty<MethodInfo>();
            var binding = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
            return t.GetMethods(binding).Concat(GetAllMethods(t.BaseType));
        }

        /// <summary>
        /// Checks if the script field should be hidden or shown upon drawing depending on attributes and settings
        /// </summary>
        /// <returns>true if the script field should be hidden otherwise false</returns>
        protected bool ShouldHideScriptField()
        {
            if (target.GetType().GetCustomAttributes<HideScriptAttribute>().Count() != 0)
            {
                return true;
            }
            else if (target.GetType().GetCustomAttributes<ShowScriptAttribute>().Count() != 0)
            {
                return false;
            }
            if (LunarisEditorSettingsWindow.Settings.DefaultHideScriptField)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Creates dynamic object(s) used for method parameter serilization.
        /// </summary>
        protected void InitMethodParameterObjects()
        {
            var typeSignature = "MyDynamicType";
            var an = new AssemblyName(typeSignature);
            AssemblyBuilder assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(an, AssemblyBuilderAccess.Run);
            ModuleBuilder moduleBuilder = assemblyBuilder.DefineDynamicModule("MainModule");

            var type = target.GetType();

            foreach (var method in type.GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
            {
                var attributes = method.GetCustomAttributes(typeof(ButtonAttribute), true);
                if (attributes.Length > 0)
                {
                    TypeBuilder tb = moduleBuilder.DefineType(typeSignature,
                        TypeAttributes.Public |
                        TypeAttributes.Class |
                        TypeAttributes.AutoClass |
                        TypeAttributes.AnsiClass |
                        TypeAttributes.BeforeFieldInit |
                        TypeAttributes.AutoLayout,
                        typeof(ScriptableObject));

                    foreach (var par in method.GetParameters())
                    {
                        tb.DefineField(par.Name, par.ParameterType, FieldAttributes.Public);
                    }
                    var tbtype = tb.CreateType();

                    var so = CreateInstance(tbtype);
                    buttonParameterObjects[method.Name + string.Concat(method.GetParameters().Select(t => t.Name))] = so;
                }
            }
        }

        protected void FindTargetProperties()
        {
            bool hasSortableArrays = false;

            listIndex.Clear();
            editableIndex.Clear();
            Type typeScriptable = typeof(ScriptableObject);

            SerializedProperty iterProp = serializedObject.GetIterator();
            // This iterator goes through all the child serialized properties, looking
            // for properties that have the SortableArray attribute
            if (iterProp.NextVisible(true))
            {
                do
                {
                    if (iterProp.isArray && iterProp.propertyType != SerializedPropertyType.String)
                    {
                        if (LunarisEditorSettingsWindow.Settings.AlwaysShowReorderableList || iterProp.HasAttribute<ReorderableAttribute>())
                        {
                            hasSortableArrays = true;
                            CreateListData(serializedObject.FindProperty(iterProp.propertyPath));
                        }
                    }

                    if (iterProp.propertyType == SerializedPropertyType.ObjectReference)
                    {
                        Type propType = iterProp.GetTypeReflection();
                        if (propType == null)
                            continue;
                        
                        if (propType.IsSubclassOf(typeScriptable))
                        {
                            if (LunarisEditorSettingsWindow.Settings.DefaultUseInlineEditor || iterProp.HasAttribute<EditScriptableAttribute>())
                            {
                                Editor scriptableEditor = null;
                                if (iterProp.objectReferenceValue != null)
                                {
                                    CreateCachedEditorWithContext(iterProp.objectReferenceValue,
                                                                serializedObject.targetObject, null,
                                                                ref scriptableEditor);

                                    var reorderable = scriptableEditor as LunarisEditor;
                                    if (reorderable != null)
                                        reorderable.isSubEditor = true;
                                }
                                editableIndex.Add(iterProp.propertyPath, scriptableEditor);
                                hasEditable = true;
                            }
                        }
                    }
                } while (iterProp.NextVisible(true));
            }

            isInitialized = true;
            if (hasSortableArrays == false)
            {
                listIndex.Clear();
            }
        }

        private void OnDisable()
        {
        }

        public override bool RequiresConstantRepaint()
        {
            return true;
        }

        public override void OnInspectorGUI()
        {
            GetSerializedProperties(ref _serializedProperties);
            
            bool anyAttribute = _serializedProperties.Any(p => PropertyUtility.GetAttribute<ILunarisAttribute>(p) != null);

                EditorGUI.BeginChangeCheck();

                DrawInspector();

                DrawButtons();

                if (EditorGUI.EndChangeCheck())
                    serializedObject.ApplyModifiedProperties();
        }

        protected void GetSerializedProperties(ref List<SerializedProperty> outSerializedProperties)
        {
            outSerializedProperties.Clear();
            using (var iterator = serializedObject.GetIterator())
            {
                if (iterator.NextVisible(true))
                {
                    do
                    {

                        outSerializedProperties.Add(serializedObject.FindProperty(iterator.name));
                    }
                    while (iterator.NextVisible(false));
                }
            }
        }

        protected virtual void DrawInspector()
        {
            foreach (var property in GetNonGroupedProperties(_serializedProperties))
            {
                if (property.name.Equals("m_Script", System.StringComparison.Ordinal))
                {
                    //using (new EditorGUI.DisabledScope(disabled: true))
                    //{
                    //    EditorGUILayout.PropertyField(property);
                    //}
                }
                else
                {
                    LunarisEditorGUI.PropertyField_Layout(property, true);
                }
            }

            SerializedProperty iterProp = serializedObject.GetIterator();
            IterateDrawProperty(iterProp);
        }


        private void CreateListData(SerializedProperty property)
        {
            string parent = EditorExtentions.GetGrandParentPath(property);

            // Try to find the grand parent in SortableListData
            SortableListData data = listIndex.Find(listData => listData.Parent.Equals(parent));
            if (data == null)
            {
                data = new SortableListData(parent);
                data.isSubEditor = isSubEditor;
                listIndex.Add(data);
            }

            data.AddProperty(property);
            object[] attr = property.GetAttributes<ReorderableAttribute>();
            if (attr != null && attr.Length == 1)
            {
                ReorderableAttribute arrayAttr = (ReorderableAttribute)attr[0];
                if (arrayAttr != null)
                {
                    HandleReorderableOptions(arrayAttr, property, data);
                }
            }
        }

        private void HandleReorderableOptions(ReorderableAttribute arrayAttr, SerializedProperty property, SortableListData data)
        {
            // Custom element header
            if (string.IsNullOrEmpty(arrayAttr.ElementHeader) == false)
            {
                data.ElementHeaderCallback = i => string.Format("{0} {1}", arrayAttr.ElementHeader, (arrayAttr.HeaderZeroIndex ? i : i + 1));
            }

            // Draw property as single line
            if (arrayAttr.ElementSingleLine)
            {
                var list = data.GetPropertyList(property);
                list.elementHeightCallback = index => EditorGUIUtility.singleLineHeight + 6;
                list.drawElementBackgroundCallback = (rect, index, active, focused) =>
                {
                    if (focused == false)
                        return;
                    if (styleHighlight == null)
                        styleHighlight = GUI.skin.FindStyle("MeTransitionSelectHead");
                    GUI.Box(rect, GUIContent.none, styleHighlight);
                };

                list.drawElementCallback = (rect, index, active, focused) =>
                {
                    var element = property.GetArrayElementAtIndex(index);
                    element.isExpanded = false;

                    int childCount = data.GetElementCount(property);
                    if (childCount < 1)
                        return;

                    rect.y += 3;
                    rect.height -= 6;

                    if (element.NextVisible(true))
                    {
                        float restoreWidth = EditorGUIUtility.labelWidth;
                        EditorGUIUtility.labelWidth /= childCount;

                        float padding = 5f;
                        float width = rect.width - padding * (childCount - 1);
                        width /= childCount;

                        Rect childRect = new Rect(rect) { width = width };
                        int depth = element.Copy().depth;
                        do
                        {
                            if (element.depth != depth)
                                break;

                            if (childCount <= 2)
                                EditorGUI.PropertyField(childRect, element, false);
                            else
                                EditorGUI.PropertyField(childRect, element, GUIContent.none, false);
                            childRect.x += width + padding;
                        } while (element.NextVisible(false));

                        EditorGUIUtility.labelWidth = restoreWidth;
                    }
                };
            }
        }

        public void DrawButtons()
        {
            if (!Application.isPlaying)
            {
                foreach (var method in target.GetType().GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
                {
                    var attributes = method.GetCustomAttributes(typeof(ButtonAttribute), true);
                    if (attributes.Length > 0)
                    {
                        SerializedObject so = new SerializedObject(buttonParameterObjects[method.Name + string.Concat(method.GetParameters().Select(t => t.Name))]);

                        EditorGUILayout.BeginHorizontal(GUI.skin.FindStyle("OL box NoExpand"));
                        EditorGUILayout.LabelField(method.Name + "(" + string.Join(", ", method.GetParameters().Select(t => t.ParameterType)) + ")");

                        if (GUILayout.Button("Invoke"))
                        {
                            List<object> obj = new List<object>();
                            var parameters = method.GetParameters();
                            object parameterobject = buttonParameterObjects[method.Name + string.Concat(method.GetParameters().Select(t => t.Name))];
                            foreach (var para in parameters)
                            {
                                obj.Add(parameterobject.GetType().GetField(para.Name).GetValue(parameterobject));
                            }
                            method.Invoke(target, obj.ToArray());
                        }
                        EditorGUILayout.EndHorizontal();
                        EditorGUILayout.Space(-5);

                        EditorGUILayout.BeginVertical(GUI.skin.FindStyle("FrameBox"));
                        var prop = so.GetIterator();
                        prop.NextVisible(true);
                        while (prop.NextVisible(true))
                        {
                            EditorGUILayout.PropertyField(prop);
                        }

                        if (GUI.changed)
                            so.ApplyModifiedProperties();

                        EditorGUILayout.EndVertical();
                    }
                }
            }
        }

        protected void IterateDrawProperty(SerializedProperty property)
        {
            if (property.NextVisible(true))
            {
                // Remember depth iteration started from
                int depth = property.Copy().depth;
                do
                {
                    // If goes deeper than the iteration depth, get out
                    if (property.depth != depth)
                        break;
                    if (isSubEditor && property.name.Equals("m_Script"))
                        continue;

                    DrawPropertySortableArray(property);
                } while (property.NextVisible(false));
            }
        }

        /// <summary>
		/// Draw a SerializedProperty as a ReorderableList if it was found during
		/// initialization, otherwise use EditorGUILayout.PropertyField
		/// </summary>
		/// <param name="property"></param>
		protected void DrawPropertySortableArray(SerializedProperty property)
        {
            // Try to get the sortable list this property belongs to
            SortableListData listData = null;
            if (listIndex.Count > 0)
                listData = listIndex.Find(data => property.propertyPath.StartsWith(data.Parent));

            Editor scriptableEditor;
            bool isScriptableEditor = editableIndex.TryGetValue(property.propertyPath, out scriptableEditor);

            // Has ReorderableList
            if (listData != null)
            {
                // Try to show the list
                if (listData.DoLayoutProperty(property) == false)
                {
                    EditorGUILayout.PropertyField(property, false);
                    if (property.isExpanded)
                    {
                        EditorGUI.indentLevel++;
                        SerializedProperty targetProp = serializedObject.FindProperty(property.propertyPath);
                        IterateDrawProperty(targetProp);
                        EditorGUI.indentLevel--;
                    }
                }
            }
            // Else try to draw ScriptableObject editor
            else if (isScriptableEditor)
            {
                bool hasHeader = property.HasAttribute<HeaderAttribute>();
                bool hasSpace = property.HasAttribute<SpaceAttribute>();

                float foldoutSpace = hasHeader ? 24 : 7;
                if (hasHeader && hasSpace)
                    foldoutSpace = 31;

                hasSpace |= hasHeader;

                // No data in property, draw property field with create button
                if (scriptableEditor == null)
                {
                    bool doCreate;
                    using (new EditorGUILayout.HorizontalScope())
                    {
                        EditorGUILayout.PropertyField(property, uiExpandWidth);
                        using (new EditorGUILayout.VerticalScope(uiWidth50))
                        {
                            if (hasSpace) GUILayout.Space(10);
                            doCreate = GUILayout.Button(labelBtnCreate, EditorStyles.miniButton);
                        }
                    }

                    if (doCreate)
                    {
                        Type propType = property.GetTypeReflection();
                        var createdAsset = CreateAssetWithSavePrompt(propType, "Assets");
                        if (createdAsset != null)
                        {
                            property.objectReferenceValue = createdAsset;
                            property.isExpanded = true;
                        }
                    }
                }
                // Has data in property, draw foldout and editor
                else
                {
                    EditorGUILayout.PropertyField(property);

                    Rect rectFoldout = GUILayoutUtility.GetLastRect();
                    rectFoldout.width = 20;
                    if (hasSpace) rectFoldout.yMin += foldoutSpace;

                    property.isExpanded = EditorGUI.Foldout(rectFoldout, property.isExpanded, GUIContent.none);

                    if (property.isExpanded)
                    {
                        EditorGUI.indentLevel++;
                        if (styleEditBox == null)
                            styleEditBox = new GUIStyle(EditorStyles.helpBox) { padding = new RectOffset(5, 5, 5, 5) };
                        if (styleEditBox == null)
                            return;

                        using (new EditorGUILayout.VerticalScope(styleEditBox))
                        {
                            var restoreIndent = EditorGUI.indentLevel;
                            EditorGUI.indentLevel = 1;
                            scriptableEditor.serializedObject.Update();
                            scriptableEditor.OnInspectorGUI();
                            scriptableEditor.serializedObject.ApplyModifiedProperties();
                            EditorGUI.indentLevel = restoreIndent;
                        }
                        EditorGUI.indentLevel--;
                    }
                }
            }
            else
            {
                SerializedProperty targetProp = serializedObject.FindProperty(property.propertyPath);

                bool isStartProp = targetProp.propertyPath.StartsWith("m_");
                using (new EditorGUI.DisabledScope(isStartProp))
                {
                    EditorGUILayout.PropertyField(targetProp, targetProp.isExpanded);
                }
            }
        }

        // Creates a new ScriptableObject via the default Save File panel
        private ScriptableObject CreateAssetWithSavePrompt(Type type, string path)
        {
            path = EditorUtility.SaveFilePanelInProject("Save ScriptableObject", "New " + type.Name + ".asset", "asset", "Enter a file name for the ScriptableObject.", path);
            if (path == "") return null;
            ScriptableObject asset = ScriptableObject.CreateInstance(type);
            AssetDatabase.CreateAsset(asset, path);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
            EditorGUIUtility.PingObject(asset);
            return asset;
        }

        private static IEnumerable<SerializedProperty> GetNonGroupedProperties(IEnumerable<SerializedProperty> properties)
        {
            return properties.Where(p => PropertyUtility.GetAttribute<IGroupAttribute>(p) == null);
        }

        private static GUIStyle styleHighlight;

        protected class SortableListData
        {
            public string Parent { get; private set; }
            public Func<int, string> ElementHeaderCallback = null;
            public bool isSubEditor;

            private readonly Dictionary<string, ReorderableList> propIndex = new Dictionary<string, ReorderableList>();
            private readonly Dictionary<string, Action<SerializedProperty, UnityEngine.Object[]>> propDropHandlers = new Dictionary<string, Action<SerializedProperty, UnityEngine.Object[]>>();
            private readonly Dictionary<string, int> countIndex = new Dictionary<string, int>();

            public SortableListData(string parent)
            {
                Parent = parent;
            }

            public void AddProperty(SerializedProperty property)
            {
                // Check if this property actually belongs to the same direct child
                if (EditorExtentions.GetGrandParentPath(property).Equals(Parent) == false)
                    return;

                ReorderableList propList = new ReorderableList(
                    property.serializedObject, property,
                    draggable: true, displayHeader: false,
                    displayAddButton: false, displayRemoveButton: false)
                {
                    headerHeight = 0,
                    footerHeight = 0,
                };

                propList.drawElementCallback = delegate (Rect rect, int index, bool active, bool focused)
                {
                    if (property.arraySize <= index)
                        return;

                    SerializedProperty targetElement = property.GetArrayElementAtIndex(index);

                    bool isExpanded = targetElement.isExpanded;
                    rect.height = EditorGUI.GetPropertyHeight(targetElement, GUIContent.none, isExpanded);

                    if (targetElement.hasVisibleChildren)
                        rect.xMin += 10;

                    // Get Unity to handle drawing each element
                    GUIContent propHeader = new GUIContent(targetElement.displayName);
                    if (ElementHeaderCallback != null)
                        propHeader.text = ElementHeaderCallback(index);
                    rect.width -= 28;
                    var tempw = EditorGUIUtility.labelWidth;
                    EditorGUIUtility.labelWidth = 0.001f;
                    EditorGUI.PropertyField(rect, targetElement, propHeader, isExpanded);
                    EditorGUIUtility.labelWidth = tempw;
                    rect.x += rect.width;
                    rect.width = 28;
                    if (GUI.Button(rect, "X"))
                    {
                        //property.DeleteArrayElementAtIndex(index);
                        property.MoveArrayElement(index, property.arraySize - 1);
                        property.arraySize--;
                        property.serializedObject.ApplyModifiedProperties();

                    }
                    // If drawing the selected element, use it to set the element height
                    // Element height seems to control selected background

                };

                //propList.list.Remove()
                

                // Unity 5.3 onwards allows reorderable lists to have variable element heights
                propList.elementHeightCallback = index => ElementHeightCallback(property, index);

                propList.drawElementBackgroundCallback = (rect, index, active, focused) =>
                {
                    if (styleHighlight == null)
                        styleHighlight = new GUIStyle();
                    if (focused == false)
                        return;
                    rect.height = ElementHeightCallback(property, index);
                    GUI.Box(rect, GUIContent.none, styleHighlight);
                };
                propIndex.Add(property.propertyPath, propList);
            }

            private float ElementHeightCallback(SerializedProperty property, int index)
            {
                if (property.arraySize <= index)
                    return 0;
                SerializedProperty arrayElement = property.GetArrayElementAtIndex(index);
                float calculatedHeight = EditorGUI.GetPropertyHeight(arrayElement,
                                                                    GUIContent.none,
                                                                    arrayElement.isExpanded);
                calculatedHeight += 3;

                return calculatedHeight;
            }

            public bool DoLayoutProperty(SerializedProperty property)
            {
                if (propIndex.ContainsKey(property.propertyPath) == false)
                    return false;

                // Draw the header
                string headerText = string.Format("{0} [{1}]", property.displayName, property.arraySize);


                GUIStyle boxStyle = new GUIStyle("ColorPickerBox");
                EditorGUILayout.Space(4);

                EditorGUILayout.BeginVertical(boxStyle);
                LunarisEditorGUI.ListHeader(property);
                Rect dropRect = GUILayoutUtility.GetLastRect();

                if (property.isExpanded || LunarisEditorSettingsWindow.Settings.ArraysAlwaysExpanded)
                {
                    propIndex[property.propertyPath].DoLayoutList();
                }
                EditorGUILayout.EndVertical();

                // Handle drag and drop into the header
                Event evt = Event.current;

                if (evt == null)
                    return true;

#if UNITY_2018_2_OR_NEWER
                if (evt.type == EventType.DragUpdated || evt.type == EventType.DragPerform)
#else
                if (evt.type == EventType.dragUpdated || evt.type == EventType.dragPerform)
#endif
                {

                    if (dropRect.Contains(evt.mousePosition))
                    {
                        DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                        if (evt.type == EventType.DragPerform)
                        {
                            DragAndDrop.AcceptDrag();
                            Action<SerializedProperty, UnityEngine.Object[]> handler = null;
                            if (propDropHandlers.TryGetValue(property.propertyPath, out handler))
                            {
                                if (handler != null)
                                    handler(property, DragAndDrop.objectReferences);
                            }
                            else
                            {
                                foreach (UnityEngine.Object dragged_object in DragAndDrop.objectReferences)
                                {
                                    if (dragged_object.GetType().Name != property.GetTypeReflection().Name)
                                        continue;

                                    int newIndex = property.arraySize;
                                    property.arraySize++;

                                    SerializedProperty target = property.GetArrayElementAtIndex(newIndex);
                                    target.objectReferenceInstanceIDValue = dragged_object.GetInstanceID();
                                    property.serializedObject.ApplyModifiedProperties();
                                }
                            }
                            evt.Use();
                        }
                    }
                }
                EditorGUILayout.Space(8);
                return true;
            }

            public int GetElementCount(SerializedProperty property)
            {
                if (property.arraySize <= 0)
                    return 0;

                int count;
                if (countIndex.TryGetValue(property.propertyPath, out count))
                    return count;

                var element = property.GetArrayElementAtIndex(0);
                var countElement = element.Copy();
                int childCount = 0;
                if (countElement.NextVisible(true))
                {
                    int depth = countElement.Copy().depth;
                    do
                    {
                        if (countElement.depth != depth)
                            break;
                        childCount++;
                    } while (countElement.NextVisible(false));
                }

                countIndex.Add(property.propertyPath, childCount);
                return childCount;
            }

            public ReorderableList GetPropertyList(SerializedProperty property)
            {
                if (propIndex.ContainsKey(property.propertyPath))
                    return propIndex[property.propertyPath];
                return null;
            }

            public void SetDropHandler(SerializedProperty property, Action<SerializedProperty, UnityEngine.Object[]> handler)
            {
                string path = property.propertyPath;
                if (propDropHandlers.ContainsKey(path))
                    propDropHandlers[path] = handler;
                else
                    propDropHandlers.Add(path, handler);
            }
        } // End SortableListData
    }
}