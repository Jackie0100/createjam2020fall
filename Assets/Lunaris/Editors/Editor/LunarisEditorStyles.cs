﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Lunaris.Editors
{
    public static class LunarisEditorStyles
    {
        public static GUIStyle FoldoutStyle;
        public static GUIStyle ToolBarHeader;
        public static GUIStyle boxStyle;
        public static GUIStyle styleEditBox;
        public static GUIStyle ToolBarButton;
        public static GUIStyle sizefieldStyle;

        public static GUIStyle PopupButton;
        public static GUIStyle EnumPickerItem;
        public static GUIStyle EnumPickerItemSelected;

        public static GUIStyle SearchTextField;



        static LunarisEditorStyles()
        {
            ToolBarHeader = new GUIStyle("TimeAreaToolbar");
            ToolBarHeader.fixedHeight = 28;
            ToolBarHeader.margin = new RectOffset(2, 2, 1, 0);
            ToolBarHeader.padding = new RectOffset(0, 0, 0, 0);

            ToolBarButton = new GUIStyle("Button");
            ToolBarButton.normal.background = Resources.Load<Texture2D>("Toolbar");
            ToolBarButton.normal.textColor = new Color32(192, 192, 192, 255);
            ToolBarButton.hover.background = Resources.Load<Texture2D>("Toolbar");

            ToolBarButton.fontStyle = FontStyle.Bold;
            ToolBarButton.fontSize = 20;

            ToolBarButton.onNormal.background = ToolBarButton.onNormal.background;
            ToolBarButton.onNormal.textColor = new Color32(255, 255, 255, 255);

            ToolBarButton.alignment = TextAnchor.MiddleCenter;
            ToolBarButton.margin = new RectOffset(0, 0, 0, 0);
            ToolBarButton.padding = new RectOffset(0, 0, 0, 0);
            ToolBarButton.border = new RectOffset(0, 0, 0, 0);

            ToolBarButton.fixedHeight = 28;
            ToolBarButton.fixedWidth = 30;
            ToolBarButton.alignment = TextAnchor.UpperCenter;
            ToolBarButton.overflow = new RectOffset(0, 0, 0, 0);
            ToolBarButton.margin = new RectOffset(0, 0, 0, 0);
            ToolBarButton.padding = new RectOffset(0, 0, 0, 0);

            FoldoutStyle = new GUIStyle(EditorStyles.foldout);
            FoldoutStyle.alignment = TextAnchor.MiddleLeft;

            EnumPickerItem = new GUIStyle();
            EnumPickerItem.alignment = TextAnchor.MiddleLeft;
            EnumPickerItem.padding = new RectOffset(32, 0, 0, 0);
            
            EnumPickerItem.normal.background = LunarisEditorColors.EnumPickerBackgroundTexture;
            EnumPickerItem.hover.background = LunarisEditorColors.EnumPickerHoverTexture;
            EnumPickerItem.normal.textColor = LunarisEditorColors.Text;
            EnumPickerItem.hover.textColor = LunarisEditorColors.Text;

            EnumPickerItemSelected = new GUIStyle();
            EnumPickerItemSelected.alignment = TextAnchor.MiddleLeft;
            EnumPickerItemSelected.padding = new RectOffset(32, 0, 0, 0);
            EnumPickerItemSelected.normal.background = LunarisEditorColors.EnumPickerSelectedTexture;
            EnumPickerItemSelected.normal.textColor = LunarisEditorColors.Text;
            EnumPickerItemSelected.hover.textColor = LunarisEditorColors.Text;

            PopupButton = new GUIStyle("Popup");

            SearchTextField = new GUIStyle("ToolbarSeachTextField");
        }
    }
}