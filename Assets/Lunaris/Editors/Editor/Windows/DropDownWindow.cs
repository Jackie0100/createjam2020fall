﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Linq;
using System;

namespace Lunaris.Editors
{
    public class EnumDropdownPicker : EditorWindow
    {
        bool allowMultiSelect;
        Type enumType;
        SerializedProperty enumProperty;
        List<SingleEnumName> enumItems;

        string searchString = "";
        Vector2 scrollpos;
        Color selectedColor = new Color32(60, 94, 145, 255);
        Color unSelectedColor = new Color(0, 0, 0, 0);
        Color textColor = new Color32(210, 210, 210, byte.MaxValue);
        Color selectedTextColor = new Color32(210, 210, 210, byte.MaxValue);

        bool firstdraw = true;
        private Texture2D selectedTex;
        private Texture2D unSelectedTex;

        const float ITEMSIZE = 20;

        public void Init(Rect pos, Type enumtype, SerializedProperty property, bool isflag)
        {
            position = new Rect(pos.x, pos.y + pos.height, pos.width, 300);
            enumType = enumtype;
            enumProperty = property;
            allowMultiSelect = isflag;

            enumItems = new List<SingleEnumName>();

            var names = Enum.GetNames(enumType);
            for (int i = 0; i < names.Length; i++)
            {
                string name = names[i];
                SingleEnumName single = new SingleEnumName();
                single.name = name;
                single.index = i;
                single.value = (int)System.Enum.Parse(enumType, name);
                if (allowMultiSelect)
                {
                    if ((enumProperty.intValue & single.value) != 0)
                    {
                        single.isSelect = true;
                    }
                    else
                    {
                        single.isSelect = false;
                    }
                }
                else
                {
                    if (enumProperty.intValue == single.value)
                    {
                        single.isSelect = true;
                    }
                }
                enumItems.Add(single);
            }
        }

        void OnGUI()
        {
            bool clicked = false;
            EditorGUILayout.BeginHorizontal();
            //GUI.DrawTexture(new Rect(0,0, position.width, position.height), LunarisEditorColors.Black, ScaleMode.StretchToFill);
            //GUI.DrawTexture(new Rect(1, 1, position.width-2, position.height-2), LunarisEditorColors.EnumPickerBackgroundTexture, ScaleMode.StretchToFill);
            GUI.SetNextControlName("SearchTextField");
            searchString = GUILayout.TextField(searchString, LunarisEditorStyles.SearchTextField);

            if (firstdraw)
                GUI.FocusControl("SearchTextField");


            EditorGUILayout.EndHorizontal();

            scrollpos = EditorGUILayout.BeginScrollView(scrollpos);
            EditorGUI.indentLevel++;

            if (allowMultiSelect)
            {
                if (GUILayout.Button("None", LunarisEditorStyles.EnumPickerItem, GUILayout.Height(ITEMSIZE)))
                {
                    foreach (var e in enumItems)
                        e.isSelect = false;

                    clicked = true;
                }
                Rect rect = GUILayoutUtility.GetLastRect();

                if (!enumItems.Any(t => t.isSelect))
                {
                    Rect r = GUILayoutUtility.GetLastRect();
                    GUI.color = Color.green;
                    r.width = 16;
                    r.x += 12;
                    GUI.DrawTexture(r, Resources.Load<Texture2D>("Checkmark"), ScaleMode.ScaleToFit);
                    GUI.color = Color.white;
                }
                rect.y += rect.height;
                rect.height = 4;
                rect.x += 16;
                rect.width -= 32;
                GUI.DrawTexture(rect, Resources.Load<Texture2D>("ShadowedDivider"), ScaleMode.StretchToFill);
                GUILayout.Space(4);
                

                if (GUILayout.Button("All", LunarisEditorStyles.EnumPickerItem, GUILayout.Height(ITEMSIZE)))
                {
                    foreach (var e in enumItems)
                        e.isSelect = true;

                    clicked = true;
                }

                rect = GUILayoutUtility.GetLastRect();

                if (enumItems.All(t => t.isSelect))
                {
                    Rect r = GUILayoutUtility.GetLastRect();
                    GUI.color = Color.green;
                    r.width = 16;
                    r.x += 12;
                    GUI.DrawTexture(r, Resources.Load<Texture2D>("Checkmark"), ScaleMode.ScaleToFit);
                    GUI.color = Color.white;
                }
                rect.y += rect.height;
                rect.height = 4;
                rect.x += 16;
                rect.width -= 32;
                GUI.DrawTexture(rect, Resources.Load<Texture2D>("ShadowedDivider"), ScaleMode.StretchToFill);
                GUILayout.Space(4);
            }


            for (int i = 0; i < enumItems.Count; i++)
            {
                if (!enumItems[i].name.ToLower().Contains(searchString.ToLower()))
                    continue;

                if (GUILayout.Button(enumItems[i].name, (enumItems[i].isSelect && !allowMultiSelect ? LunarisEditorStyles.EnumPickerItemSelected : LunarisEditorStyles.EnumPickerItem), GUILayout.Height(ITEMSIZE)))
                {
                    if (!allowMultiSelect)
                    {
                        if (enumItems.Where(t => t.isSelect).Any())
                            foreach (var e in enumItems)
                                e.isSelect = false;
                    }

                    enumItems[i].isSelect = !enumItems[i].isSelect;
                    clicked = true;
                }
                Rect rect = GUILayoutUtility.GetLastRect();
                if (enumItems[i].isSelect)
                {
                    Rect r = GUILayoutUtility.GetLastRect();
                    GUI.color = Color.green;
                    r.width = 16;
                    r.x += 12;
                    GUI.DrawTexture(r, Resources.Load<Texture2D>("Checkmark"), ScaleMode.ScaleToFit);
                    GUI.color = Color.white;
                }
                if (i + 1 != enumItems.Count)
                {
                    rect.y += rect.height;
                    rect.height = 4;
                    rect.x += 16;
                    rect.width -= 32;
                    GUI.DrawTexture(rect, Resources.Load<Texture2D>("ShadowedDivider"), ScaleMode.StretchToFill);
                    GUILayout.Space(4);
                }
            }

            if (clicked)
            {
                if (enumItems.Where(t => t.isSelect).Sum(t => t.value) + 1 == (1 << enumItems.Count))
                {
                    enumProperty.intValue = -1;
                }
                else
                {
                    enumProperty.intValue = enumItems.Where(t => t.isSelect).Sum(t => t.value);
                }
                enumProperty.serializedObject.ApplyModifiedProperties();
            }

            EditorGUI.indentLevel--;
            EditorGUILayout.EndScrollView();
            this.Repaint();
        }

        private void OnLostFocus()
        {
            this.Close();
        }

        private class SingleEnumName
        {
            public string name;
            public int index;
            public int value;
            public bool isSelect;
        }
    }
}