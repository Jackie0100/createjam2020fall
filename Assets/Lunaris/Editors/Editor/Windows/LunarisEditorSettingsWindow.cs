﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.IO;

namespace Lunaris.Editors.Windows
{

    internal class LunarisEditorSettingsWindow : EditorWindow
    {
        public static LunarisEditorSettings Settings { get; private set; }

        static LunarisEditorSettingsWindow()
        {
            if (File.Exists("Assets/Lunaris/Editors/Editor/Resources/LunarisSettings.json"))
            {
                Settings = JsonUtility.FromJson<LunarisEditorSettings>(File.ReadAllText("Assets/Lunaris/Editors/Editor/Resources/LunarisSettings.json"));
            }
            else
            {
                Settings = new LunarisEditorSettings()
                {
                    AlwaysShowReorderableList = true,
                    ArraysAlwaysExpanded = false,
                    DefaultHideScriptField = false,
                    DefaultUseInlineEditor = true,
                    HideElementCountField = false,
                    InlineEditorAlwaysExpanded = false,
                    ShowCreateButtonForEmptyScriptableObjectReferences = true,
                    ShowReorderableListForNestedObjects = true,
                    UseInlineEditorInLists = true,
                    UseLunarisEnumPickerDefault = true,
                };
            }
        }

        [MenuItem("Lunaris/Editor Settings")]
        static void Init()
        {
            LunarisEditorSettingsWindow window = ScriptableObject.CreateInstance<LunarisEditorSettingsWindow>();
            window.position = new Rect(Screen.width / 2, Screen.height / 2, 250, 400);
            window.ShowUtility();
        }

        private void OnEnable()
        {
            try
            {
                if (File.Exists("Assets/Lunaris/Editors/Editor/Resources/LunarisSettings.json"))
                {
                    Settings = JsonUtility.FromJson<LunarisEditorSettings>(File.ReadAllText("Assets/Lunaris/Editors/Editor/Resources/LunarisSettings.json"));
                }
                else
                {
                    Settings = new LunarisEditorSettings()
                    {
                        AlwaysShowReorderableList = true,
                        ArraysAlwaysExpanded = false,
                        DefaultHideScriptField = false,
                        DefaultUseInlineEditor = true,
                        HideElementCountField = false,
                        InlineEditorAlwaysExpanded = false,
                        ShowCreateButtonForEmptyScriptableObjectReferences = true,
                        ShowReorderableListForNestedObjects = true,
                        UseInlineEditorInLists = true,
                        UseLunarisEnumPickerDefault = true,
                    };
                }
            }
            catch
            {
                Debug.LogWarning("No Lunaris editor setting file found, generating a new one...");
                Settings = new LunarisEditorSettings()
                {
                    AlwaysShowReorderableList = true,
                    ArraysAlwaysExpanded = false,
                    DefaultHideScriptField = false,
                    DefaultUseInlineEditor = true,
                    HideElementCountField = false,
                    InlineEditorAlwaysExpanded = false,
                    ShowCreateButtonForEmptyScriptableObjectReferences = true,
                    ShowReorderableListForNestedObjects = true,
                    UseInlineEditorInLists = true,
                    UseLunarisEnumPickerDefault = true,
                };
            }
        }

        private void OnGUI()
        {
            var image = EditorGUIUtility.Load("Assets/Lunaris/Editors/Editor/Resources/placeholder.png") as Texture2D;
            GUILayout.Label("Lunaris Editors Settings");
            GUI.DrawTexture(new Rect(0,0, 100,100), image);
             
            EditorGUILayout.Space(120);

            EditorGUIUtility.labelWidth = 400;
            Settings.AlwaysShowReorderableList = EditorGUILayout.Toggle("Replace unity standards list with reordeable lists always.", Settings.AlwaysShowReorderableList);
            Settings.ShowReorderableListForNestedObjects = EditorGUILayout.Toggle("Replace unity standards list with reordeable lists for nested items.", Settings.ShowReorderableListForNestedObjects);

            Settings.ArraysAlwaysExpanded = EditorGUILayout.Toggle("Forces arrays and lists to always be expanded and removes the toggle.", Settings.ArraysAlwaysExpanded);
            Settings.HideElementCountField = EditorGUILayout.Toggle("Hide the count field indicating the numbers of items in a list.", Settings.HideElementCountField);

            Settings.DefaultUseInlineEditor = EditorGUILayout.Toggle("Always show inline editor for scriptableobjects where possible.", Settings.DefaultUseInlineEditor);
            Settings.UseInlineEditorInLists = EditorGUILayout.Toggle("Use inline editors in lists and arrays.", Settings.UseInlineEditorInLists);
            Settings.InlineEditorAlwaysExpanded = EditorGUILayout.Toggle("Inline editors is always expanded and removes the toggle.", Settings.InlineEditorAlwaysExpanded);

            Settings.ShowCreateButtonForEmptyScriptableObjectReferences = EditorGUILayout.Toggle("Show create button when reference for a scriptableobject is empty.", Settings.ShowCreateButtonForEmptyScriptableObjectReferences);

            Settings.DefaultHideScriptField = EditorGUILayout.Toggle("Always hide the \"Script\" field as default", Settings.DefaultHideScriptField);

            Settings.UseLunarisEnumPickerDefault = EditorGUILayout.Toggle("Replace unity's enum picker with the Lunaris Enum-picker.", Settings.UseLunarisEnumPickerDefault); 
        }

        private void OnDisable()
        {
            if (Settings != null)
            {
                string newjson = JsonUtility.ToJson(Settings);
                System.IO.File.WriteAllText("Assets/Lunaris/Editors/Editor/Resources/LunarisSettings.json", newjson);
            }
        }
    }

    [System.Serializable]
    internal class LunarisEditorSettings
    {
        [SerializeField]
        private bool _alwaysShowReorderableList;
        [SerializeField]
        private bool _showReorderableListForNestedObjects;
        [SerializeField]
        private bool _arraysAlwaysExpanded;
        [SerializeField]
        private bool _hideElementCountField;
        [SerializeField]
        private bool _defaultUseInlineEditor;
        [SerializeField]
        private bool _useInlineEditorInLists;
        [SerializeField]
        private bool _inlineEditorAlwaysExpanded;
        [SerializeField]
        private bool _showCreateButtonForEmptyScriptableObjectReferences;
        [SerializeField]
        private bool _defaultHideScriptField;
        [SerializeField]
        private bool _useLunarisEnumPickerDefault;

        public bool ArraysAlwaysExpanded
        {
            get
            {
                return _arraysAlwaysExpanded;
            }
            set
            {
                _arraysAlwaysExpanded = value;
            }
        }
        public bool HideElementCountField
        {
            get
            {
                return _hideElementCountField;
            }
            set
            {
                _hideElementCountField = value;
            }
        }

        public bool DefaultUseInlineEditor
        {
            get
            {
                return _defaultUseInlineEditor;
            }
            set
            {
                _defaultUseInlineEditor = value;
            }
        }
        public bool UseInlineEditorInLists
        {
            get
            {
                return _useInlineEditorInLists;
            }
            set
            {
                _useInlineEditorInLists = value;
            }
        }
        public bool InlineEditorAlwaysExpanded
        {
            get
            {
                return _inlineEditorAlwaysExpanded;
            }
            set
            {
                _inlineEditorAlwaysExpanded = value;
            }
        }

        public bool ShowCreateButtonForEmptyScriptableObjectReferences
        {
            get
            {
                return _showCreateButtonForEmptyScriptableObjectReferences;
            }
            set
            {
                _showCreateButtonForEmptyScriptableObjectReferences = value;
            }
        }

        public bool DefaultHideScriptField
        {
            get
            {
                return _defaultHideScriptField;
            }
            set
            {
                _defaultHideScriptField = value;
            }
        }

        public bool UseLunarisEnumPickerDefault
        {
            get
            {
                return _useLunarisEnumPickerDefault;
            }
            set
            {
                _useLunarisEnumPickerDefault = value;
            }
        }

        public bool ShowReorderableListForNestedObjects
        {
            get
            {
                return _showReorderableListForNestedObjects;
            }

            set
            {
                _showReorderableListForNestedObjects = value;
            }
        }

        public bool AlwaysShowReorderableList
        {
            get
            {
                return _alwaysShowReorderableList;
            }

            set
            {
                _alwaysShowReorderableList = value;
            }
        }
    }
}
