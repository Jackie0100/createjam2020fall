﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

//[CustomPropertyDrawer(typeof(ReorderableAttribute), true)]
public class ReorderableDrawer : PropertyDrawer
{
    ReorderableList list;


    void DrawListItems(Rect rect, int index, bool isActive, bool isFocused)
    {
        SerializedProperty element = list.serializedProperty.GetArrayElementAtIndex(index); //The element in the list


    }

    void DrawHeader(Rect rect)
    {
        string name = "Wave";
        EditorGUI.LabelField(rect, name);
    }

    //This is the function that makes the custom editor work
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (list == null)
        {
            /*var t = property.FindPropertyRelative("_testerarraylist");
            list = new ReorderableList((IList)property.serializedObject.targetObject, property.serializedObject.targetObject.GetType(), true, true, true, true);
            list.drawElementCallback = DrawListItems;
            list.drawHeaderCallback = DrawHeader;*/
        }
        //TODO: Lidt.List is not getting any items????
        if (list.list == null)
            return;
        list.DoLayoutList();
    }
}

