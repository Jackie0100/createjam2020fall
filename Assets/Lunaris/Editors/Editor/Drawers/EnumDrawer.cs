﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Lunaris.Editors
{
    [CustomPropertyDrawer(typeof(Enum), true)]
    public class EnumDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (Lunaris.Editors.Windows.LunarisEditorSettingsWindow.Settings.UseLunarisEnumPickerDefault)
            {
                EditorGUI.BeginProperty(position, label, property);
                GUI.Label(new Rect(position.x, position.y, EditorGUIUtility.labelWidth, position.height), property.displayName);
                BindingFlags bindingFlags = System.Reflection.BindingFlags.GetField | System.Reflection.BindingFlags.GetProperty | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Public;
                var enumtype = property.serializedObject.targetObject.GetType().GetField(property.name, bindingFlags).FieldType;
                var ismaskEnum = enumtype.CustomAttributes.Any(t => t.AttributeType == typeof(FlagsAttribute));
                string buttonstring = "";
                if (ismaskEnum)
                {
                    if (property.intValue == 0)
                    {
                        buttonstring = "None";
                    }
                    else if (property.intValue == -1)
                    {
                        buttonstring = "All";
                    }
                    else
                    {
                        var names = Enum.GetNames(enumtype).Where(t => (property.intValue & (int)System.Enum.Parse(enumtype, t)) != 0).ToArray();
                        for (int i = 0; i < names.Length; i++)
                        {
                            if (LunarisEditorStyles.PopupButton.CalcSize(new GUIContent(buttonstring + names[i] + " (and " + (names.Length - i) + " more)")).x > (position.width - EditorGUIUtility.labelWidth - 20))
                            {
                                buttonstring += " (and " + (names.Length - i) + " more)";
                                break;
                            }
                            else
                            {
                                buttonstring += names[i];
                            }
                            if (i + 1 != names.Length)
                            {
                                buttonstring += ", ";
                            }
                        }
                    }
                }
                else
                {
                    buttonstring = Enum.GetName(enumtype, property.intValue);
                }
                if (GUI.Button(new Rect(position.x + EditorGUIUtility.labelWidth, position.y, position.width - EditorGUIUtility.labelWidth, position.height), buttonstring, LunarisEditorStyles.PopupButton))
                {
                    Rect r = new Rect(GUIUtility.GUIToScreenRect(position).position + new Vector2(EditorGUIUtility.labelWidth, 0), new Vector2(position.width - EditorGUIUtility.labelWidth, position.height));

                    EnumDropdownPicker window = EnumDropdownPicker.CreateInstance<EnumDropdownPicker>();
                    window.Init(r, enumtype, property, ismaskEnum);
                    window.ShowPopup();
                }
                EditorGUI.EndProperty();
            }
            else
            {
                EditorGUI.PropertyField(position, property);
            }
        }
    }
}