﻿using System;
namespace Lunaris.Attributes
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public sealed class HideScriptAttribute : Attribute, ILunarisAttribute { }
}