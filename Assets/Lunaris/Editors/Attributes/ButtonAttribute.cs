﻿using System;
namespace Lunaris.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class ButtonAttribute : Attribute, ILunarisAttribute { }
}