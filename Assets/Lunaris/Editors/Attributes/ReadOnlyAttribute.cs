﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Lunaris.Attributes
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class ReadOnlyAttribute : PropertyAttribute, ILunarisAttribute { }
}