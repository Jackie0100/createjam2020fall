﻿using System;
namespace Lunaris.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class MinMaxRangeAttribute : Attribute
    {
        float min;
        float max;

        public MinMaxRangeAttribute(float min, float max)
        {
            Min = min;
            Max = max;
        }

        public float Min
        {
            get
            {
                return min;
            }
            private set
            {
                min = value;
            }
        }

        public float Max
        {
            get
            {
                return max;
            }
            private set
            {
                max = value;
            }
        }
    }
}