﻿using Lunaris.Attributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Lunaris.ScriptableVariables;
using System;

[System.Serializable]
[HideScript]
public class EditorTest : MonoBehaviour
{
    [SerializeField]
    FlagEnum keys;
    [SerializeField]
    NormalEnum SingleKey;
    [SerializeField]
    float _floatval;
    [SerializeField]
    Color _color;
    [SerializeField]
    [ReadOnly]
    int _intval;
    GameObject _gameObject;
    [SerializeField]
    System.Single _single;
    [SerializeField]
    [Reorderable(isZeroIndex: true, isSingleLine: false)]
    [EditScriptable]
    public List<GameObjectCollection> _testerarraylist = new List<GameObjectCollection>() { };
    [SerializeField]
    [Reorderable(isZeroIndex: true, isSingleLine: false)]
    [EditScriptable]
    public GameObjectCollection[] _testerarray;
    [EditScriptable]
    public GameObjectCollection _goc;
    //public UnityEvent _event;



    [Button]
    public void PrintMessage(string message, Color color)
    {
        Debug.Log(message + color.ToString());
    }
}

[Flags]
public enum FlagEnum
{
    A = 1,
    B = 2,
    C = 4,
    D = 8,
    E = 16,
    F = 32,
    G = 64,
    H = 128,
    I = 256,
    J = 512,
    K = 1024,
    L = 2048,
    M = 4096,
    N = 8192,
    O = 16384,
    P = 32768,
    Q = 65536,
    R = 131072,
    S = 262144,
    T = 524288,
    U = 1048576,
    V = 2097152,
    W = 4194304,
    X = 8388608,
    Y = 16777216,
    Z = 33554432,
}

public enum NormalEnum
{
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L,
    M,
    N,
    O,
    P,
    Q,
    R,
    S,
    T,
    U,
    V,
    W,
    X,
    Y,
    Z,
}