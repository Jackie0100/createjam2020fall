﻿//using System.Collections;
//using System.Linq;
//using System.Collections.Generic;
//using System.Text;
//using UnityEditor;
//using UnityEditor.UIElements;
//using UnityEditorInternal;
//using UnityEngine;
//using UnityEngine.UIElements;
//using System;

//[CustomEditor(typeof(EditorTest))]
//public class EditorTestEditor : Editor
//{
//    //The array property we will edit

//    //The Reorderable list we will be working with
//    //ReorderableList list;
//    EditorTest m_BindingsTestObject;
//    VisualElement root;
//    ListView lv;

//    public override VisualElement CreateInspectorGUI()
//    {
//        root = new VisualElement();

//        m_BindingsTestObject = (EditorTest)target;

//        const int itemCount = 1000;
//        var items = new List<string>(itemCount);
//        for (int i = 1; i <= itemCount; i++)
//            items.Add(i.ToString());

//        Func<VisualElement> makeItem = () => new Label();
//        Action<VisualElement, int> bindItem = (e, i) => { (e as Label).text = (m_BindingsTestObject._testerarraylist[i] != null ? m_BindingsTestObject._testerarraylist[i].name : "NULL"); (e as Label).RegisterCallback<MouseMoveEvent>(DoestDrag); };
//        DropContainer dropzone = new DropContainer();
//        dropzone.style.height = 100;
//        DragContainer dc = new DragContainer();
//        dc.style.height = 50;
//        dc.Add(new Label("Test Label"));
//        dropzone.Add(dc);
//        root.Add(dropzone);
//        //lv = new ListView();
//        ////lv.itemsSource = m_BindingsTestObject._testerarraylist;
//        //lv.BindProperty(serializedObject.FindProperty("_testerarraylist"));
//        //lv.makeItem = makeItem;
//        //lv.bindItem = bindItem;
//        //lv.reorderable = true;
//        //lv.pickingMode = PickingMode.Position;
//        //lv.focusable = true;
//        //lv.style.flexGrow = 1;
//        //lv.style.minHeight = 200;
//        //lv.selectionType = SelectionType.Single;
//        //lv.itemHeight = 32;
//        //root.Add(lv);
//        //lv.Refresh();
//        return root;
//    }

//    private void DoestDrag(MouseMoveEvent evt)
//    {

//        (evt.target as Label).style.top = (evt.target as Label).style.top.value.value + evt.mouseDelta.y;
//    }

//    private VisualElement MakeArrayItems()
//    {
//        var listitem = new VisualElement();
//        return new Label();
//    }

//    private void ListviewBinding(VisualElement arg1, int arg2)
//    {
//        (arg1 as Label).text = (m_BindingsTestObject._testerarraylist[arg2] != null ? m_BindingsTestObject._testerarraylist[arg2].name : "NULL");
//    }

//    void BindButton(string name, Action clickEvent)
//    {
//        var button = root.Q<Button>(name);

//        if (button != null)
//        {
//            button.clickable.clicked += clickEvent;
//        }
//    }
//    private void LogValues()
//    {
//        string logStr = "";
//        foreach (var v in m_BindingsTestObject._testerarraylist)
//        {
//            if (v != null)
//                logStr += v.name;
//        }
//        Debug.LogWarning(string.Format("ObjectValues: {0}", logStr));
//    }
//}

//public class test : VisualElement
//{
//}